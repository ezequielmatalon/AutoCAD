package com.appdirect.connector.autodesk.rest.model;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.appdirect.connector.autodesk.model.AutodeskProduct;
import com.appdirect.connector.autodesk.rest.model.common.RevokeResponse;
import com.appdirect.connector.autodesk.rest.model.order.cancel.CancelOrderRequest;
import com.appdirect.connector.autodesk.rest.model.order.common.LineItem;
import com.appdirect.connector.autodesk.rest.model.order.details.Element;
import com.appdirect.connector.autodesk.rest.model.order.details.Message;
import com.appdirect.connector.autodesk.rest.model.order.details.OrderDetailsResponse;
import com.appdirect.connector.autodesk.rest.model.order.details.OrderHeaderArray;
import com.appdirect.connector.autodesk.rest.model.order.details.OrderItemsArray;
import com.appdirect.connector.autodesk.rest.model.order.place.EndUser;
import com.appdirect.connector.autodesk.rest.model.order.place.PlaceOrderRequest;
import com.appdirect.connector.autodesk.rest.model.order.place.PlaceOrderResponse;
import com.appdirect.connector.autodesk.rest.model.order.status.OrderStatusResponse;
import com.appdirect.connector.autodesk.rest.model.products.add.AddProductsRequest;
import com.appdirect.connector.autodesk.rest.model.products.add.AddProductsResponse;
import com.appdirect.connector.autodesk.rest.model.products.price.Price;
import com.appdirect.connector.autodesk.rest.model.products.price.PriceResponse;
import com.appdirect.connector.autodesk.rest.model.products.remove.RemoveProductsRequest;
import com.appdirect.connector.autodesk.rest.model.seats.add.AddSeatsRequest;
import com.appdirect.connector.autodesk.rest.model.seats.add.AddSeatsResponse;
import com.appdirect.connector.autodesk.rest.model.seats.remove.RemoveSeatsRequest;
import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.PojoValidator;
import com.openpojo.validation.rule.impl.GetterMustExistRule;
import com.openpojo.validation.rule.impl.SetterMustExistRule;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;

public class RestModelTest {

	private PojoValidator pojoValidator;

	@BeforeMethod
	public void setUp() {
		pojoValidator = new PojoValidator();
	}

	@Test
	public void testOrderItemsArray_getterAndSetter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(OrderItemsArray.class);
		addGetterValidator();
		addSetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testOrderHeaderArray_getterAndSetter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(OrderHeaderArray.class);
		addGetterValidator();
		addSetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testElement_getterAndSetter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(Element.class);
		addGetterValidator();
		addSetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testMessage_getterAndSetter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(Message.class);
		addGetterValidator();
		addSetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testAccessToken_getterAndSetter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(AccessToken.class);
		addGetterValidator();
		addSetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testAutodeskError_getter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(AutodeskError.class);
		addGetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testRevokeResponse_getterAndSetter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(RevokeResponse.class);
		addGetterValidator();
		addSetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testAutodeskProduct_getterAndSetter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(AutodeskProduct.class);
		addGetterValidator();
		addSetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testLineItem_getterAndSetter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(LineItem.class);
		addGetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testEndUser_getter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(EndUser.class);
		addGetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testPrice_getterAndSetter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(Price.class);
		addGetterValidator();
		addSetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testPriceResponse_getterAndSetter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(PriceResponse.class);
		addGetterValidator();
		addSetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testCancelOrderRequest_getter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(CancelOrderRequest.class);
		addGetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testOrderDetailsResponse_getterAndSetter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(OrderDetailsResponse.class);
		addGetterValidator();
		addSetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testPlaceOrderRequest_getter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(PlaceOrderRequest.class);
		addGetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testOrderStatusResponse_getterAndSetter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(OrderStatusResponse.class);
		addGetterValidator();
		addSetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testPlaceOrderResponse_getterAndSetter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(PlaceOrderResponse.class);
		addGetterValidator();
		addSetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testAddProductsRequest_getter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(AddProductsRequest.class);
		addGetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testAddProductsResponse_getterAndSetter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(AddProductsResponse.class);
		addGetterValidator();
		addSetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testAddSeatsRequest_getter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(AddSeatsRequest.class);
		addGetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testAddSeatsResponse_getterAndSetter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(AddSeatsResponse.class);
		addGetterValidator();
		addSetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testRemoveProductsRequest_getter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(RemoveProductsRequest.class);
		addGetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testRemoveSeatsRequest_getter() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(RemoveSeatsRequest.class);
		addGetterValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testCancelOrderRequestBuilder_toString() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(CancelOrderRequest.CancelOrderRequestBuilder.class);
		addToStringValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testLineItemBuilder_toString() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(LineItem.LineItemBuilder.class);
		addToStringValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testAddProductsRequestBuilder_toString() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(AddProductsRequest.AddProductsRequestBuilder.class);
		addToStringValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testRemoveProductsRequestBuilder_toString() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(RemoveProductsRequest.RemoveProductsRequestBuilder.class);
		addToStringValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testAddSeatsRequestBuilder_toString() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(AddSeatsRequest.AddSeatsRequestBuilder.class);
		addToStringValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testRemoveSeatsRequestBuilder_toString() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(RemoveSeatsRequest.RemoveSeatsRequestBuilder.class);
		addToStringValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testPlaceOrderRequestBuilder_toString() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(PlaceOrderRequest.PlaceOrderRequestBuilder.class);
		addToStringValidator();
		pojoValidator.runValidation(personPojo);
	}

	@Test
	public void testEndUserBuilder_toString() {
		PojoClass personPojo = PojoClassFactory.getPojoClass(EndUser.EndUserBuilder.class);
		addToStringValidator();
		pojoValidator.runValidation(personPojo);
	}

	private void addGetterValidator() {
		pojoValidator.addRule(new GetterMustExistRule());
		pojoValidator.addTester(new GetterTester());
	}

	private void addSetterValidator() {
		pojoValidator.addRule(new SetterMustExistRule());
		pojoValidator.addTester(new SetterTester());
	}

	private void addToStringValidator() {
		pojoValidator.addTester(new ToStringTester());
	}
}
