package com.appdirect.connector.autodesk.rest.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.appdirect.connector.autodesk.exception.AutodeskAPIException;
import com.appdirect.connector.autodesk.exception.AutodeskResponseConvertException;
import com.appdirect.connector.autodesk.rest.model.AutodeskError;
import com.appdirect.connector.autodesk.rest.model.order.details.Message;
import com.appdirect.connector.autodesk.rest.model.order.details.OrderDetailsResponse;
import com.appdirect.connector.autodesk.rest.model.order.place.PlaceOrderResponse;
import com.appdirect.connector.autodesk.type.StatusType;
import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class ConverterTest {
	
	@Mock
	private ObjectMapper mapper;
	
	@InjectMocks
	private OrderDetailsConverter orderDetailsConverter;
	
	@InjectMocks
	private DefaultConverter defaultConverter;
	
	@BeforeMethod
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test(description = "Tests convertion from a successful Autodesk API response to an OrderDetailResponse by the OrderDetailsConverter")
	public void testProcessOrderDetailsConverter_success() throws Throwable {
				
		OrderDetailsResponse orderDetailsResponseMock = Mockito.mock(OrderDetailsResponse.class);
		
		when(orderDetailsResponseMock.isError()).thenReturn(false);
		when(orderDetailsResponseMock.getMessage()).thenReturn(new Message());

		Response<OrderDetailsResponse> retrofitSuccessResponse = Response.success(orderDetailsResponseMock);
		
		OrderDetailsResponse orderDetailsResponse = orderDetailsConverter.process(retrofitSuccessResponse);
		
		assertThat(orderDetailsResponse.isError()).isEqualTo(false);
		assertThat(orderDetailsResponse.getMessage()).isNotNull();
		assertThat(orderDetailsResponse.getErrorMessage()).isNull();
	}
	
	@Test(expectedExceptions = AutodeskAPIException.class, 
			description = "Tests that a properly formed AutodeskAPIException is thrown by the OrderDetailsConverter when the Autodesk API does not return any order details")
	public void testProcessOrderDetailsConverter_AutodeskAPIException() throws Throwable {
		
		OrderDetailsResponse orderDetailsResponseMock = Mockito.mock(OrderDetailsResponse.class);
		
		when(orderDetailsResponseMock.isError()).thenReturn(true);
		when(orderDetailsResponseMock.getErrorMessage()).thenReturn("The search did not yield any results");
		when(orderDetailsResponseMock.getStatus()).thenReturn(StatusType.OK);
		
		Response<OrderDetailsResponse> retrofitSuccessResponse = Response.success(orderDetailsResponseMock);
		
		try {
			orderDetailsConverter.process(retrofitSuccessResponse);
		} catch (Throwable e) {
			AutodeskAPIException apiException = (AutodeskAPIException) e;
			
			assertThat(apiException.getCode()).isEqualTo(HttpStatus.NOT_FOUND.value());
			assertThat(apiException.getStatus()).isEqualTo("OK");
			assertThat(apiException.getMessage()).isEqualTo("The search did not yield any results");
			
			throw apiException;
		}
	}
	
	@Test(expectedExceptions = AutodeskAPIException.class, 
			description = "Tests that a properly formed AutodeskAPIException is thrown by the OrderDetailsConverter when the Autodesk API server returns an error code")
	public void testProcessOrderDetailsConverter_AutodeskServerError() throws Throwable {
		
		setUpAutodeskErrorTest();
		Response<ResponseBody> retrofitErrorResponse = buildRetrofitErrorResponse();
		
		try {
			orderDetailsConverter.process(retrofitErrorResponse);
		} catch (Throwable e) {
			AutodeskAPIException apiException = (AutodeskAPIException) e;
			
			assertThat(apiException.getCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.value());
			assertThat(apiException.getStatus()).isEqualTo("FAILED");
			assertThat(apiException.getMessage()).isEqualTo("error message");
			
			throw apiException;
		}
	}
	
	@Test(description = "Tests convertion from a successful Autodesk API response to a PlaceOrderResponse by the DefaultConverter")
	public void testProcessDefaultConverter_success() throws Throwable {
		
		PlaceOrderResponse placeOrderResponseMock = Mockito.mock(PlaceOrderResponse.class);
		when(placeOrderResponseMock.getStatus()).thenReturn(StatusType.OK);

		Response<PlaceOrderResponse> retrofitSuccessResponse = Response.success(placeOrderResponseMock);
		
		PlaceOrderResponse placeOrderResponse = defaultConverter.process(retrofitSuccessResponse);
		
		assertThat(placeOrderResponse.getStatus()).isEqualTo(StatusType.OK);
	}
	
	@Test(expectedExceptions = AutodeskResponseConvertException.class,
			description = "Tests that a properly formed AutodeskResponseConvertException is thrown by the DefaultConverter when an error occurs inside the failure() method")
	public void testProcessDefaultConverter_AutodeskResponseConvertException() throws Throwable {
		
		Response<ResponseBody> retrofitErrorResponse = buildRetrofitErrorResponse();

		when(mapper.readValue(Mockito.anyString(), eq(AutodeskError.class))).thenThrow(new IOException("IO exception thrown"));
		
		try {
			defaultConverter.process(retrofitErrorResponse);
		} catch (Throwable e) {
			AutodeskResponseConvertException converterException = (AutodeskResponseConvertException) e;
			
			assertThat(converterException.getCause()).isInstanceOf(IOException.class);
			assertThat(converterException.getMessage()).isEqualTo("java.io.IOException: IO exception thrown");
			
			throw converterException;
		}
	}
	
	@Test(expectedExceptions = AutodeskAPIException.class,
			description = "Tests that a properly formed AutodeskAPIException is thrown by the DefaultConverter when the Autodesk API server returns an error code")
	public void testProcessDefaultConverter_AutodeskServerError() throws Throwable {
		
		setUpAutodeskErrorTest();
		Response<ResponseBody> retrofitErrorResponse = buildRetrofitErrorResponse();
		
		try {
			defaultConverter.process(retrofitErrorResponse);
		} catch (Throwable e) {
			AutodeskAPIException apiException = (AutodeskAPIException) e;
			
			assertThat(apiException.getCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.value());
			assertThat(apiException.getStatus()).isEqualTo("FAILED");
			assertThat(apiException.getMessage()).isEqualTo("error message");
			
			throw apiException;
		}
	}
	
	private void setUpAutodeskErrorTest() throws Throwable {
		AutodeskError error = Mockito.mock(AutodeskError.class);
		when(mapper.readValue(Mockito.anyString(), eq(AutodeskError.class))).thenReturn(error);
		when(error.getStatus()).thenReturn("FAILED");
		when(error.getMessage()).thenReturn("error message");
	}
	
	private Response<ResponseBody> buildRetrofitErrorResponse() {
		ResponseBody noResultsResponseBody = ResponseBody.create(MediaType.parse("application/json"), "{\"status\": \"FAILED\", \"message\": \"error message\"}");
		Response<ResponseBody> retrofitErrorResponse = Response.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), noResultsResponseBody);
		return retrofitErrorResponse;
	}
	
}
