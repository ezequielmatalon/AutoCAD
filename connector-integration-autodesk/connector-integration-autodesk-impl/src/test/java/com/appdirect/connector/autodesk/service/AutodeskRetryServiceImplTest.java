package com.appdirect.connector.autodesk.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import org.assertj.core.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.test.util.ReflectionTestUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.appdirect.connector.autodesk.exception.AutodeskRetryServiceException;
import com.appdirect.connector.autodesk.rest.model.AutodeskResponse;

public class AutodeskRetryServiceImplTest {

    private static final int MILLISECONDS_TO_RETRY = 10;
    private static final int MAX_RETRIES = 10;

    @InjectMocks
    @Spy
    private AutodeskRetryServiceImpl service;

    @Mock
    private Callable<AutodeskResponse> callable;

    @Mock
    private Consumer<AutodeskResponse> onSuccess;

    @Mock
    private ResponseCallable doneCondition;

    @Mock
    private ResponseCallable abortCondition;

    @Mock
    private FailureCallable retryCondition;

    private Executor asyncExecutor = Executors.newSingleThreadExecutor();

    @BeforeMethod
    public void setUpMethod() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(service, "asyncExecutor", asyncExecutor);
        ReflectionTestUtils.setField(service, "millisecondsToRetry", MILLISECONDS_TO_RETRY);
        ReflectionTestUtils.setField(service, "maxRetries", MAX_RETRIES);
    }

	@Test(description = "Testing the success behavior of the CompletableFuture returned by the service")
    public void testGetAsyncResponse_success() throws Throwable {
        CompletableFuture<AutodeskResponse> future;
        AutodeskResponse response = null;

        when(callable.call()).thenReturn(response);
        when(doneCondition.call(response)).thenReturn(true);
        when(abortCondition.call(response)).thenReturn(false);

        future = service.getAsyncResponse(callable, onSuccess, doneCondition, abortCondition);
        response = future.get();

        verify(doneCondition, times(2)).call(response);
        verify(abortCondition, times(1)).call(response);
        verify(onSuccess, times(1)).accept(response);
    }

    @Test(description = "Testing the fail behavior of the CompletableFuture returned by the service")
    public void testGetAsyncResponse_fail() throws Throwable {
        CompletableFuture<AutodeskResponse> future;
        AutodeskResponse response = null;

        when(callable.call()).thenReturn(response);
        when(doneCondition.call(response)).thenReturn(false);
        when(abortCondition.call(response)).thenReturn(true);

        future = service.getAsyncResponse(callable, onSuccess, doneCondition, abortCondition);
        response = future.get();

        verify(doneCondition, times(2)).call(response);
        verify(abortCondition, times(1)).call(response);
        verify(onSuccess, times(0)).accept(response);
    }

    @Test(description = "Testing the retry and success behavior of the CompletableFuture returned by the service")
    public void testGetAsyncResponse_retrySuccess() throws Throwable {
        CompletableFuture<AutodeskResponse> future;
        AutodeskResponse response = null;

        when(callable.call()).thenReturn(response);
        when(doneCondition.call(response)).thenReturn(false, false, false, true);
        when(abortCondition.call(response)).thenReturn(false, false, false, false);

        future = service.getAsyncResponse(callable, onSuccess, doneCondition, abortCondition);
        response = future.get();

        verify(doneCondition, times(5)).call(response);
        verify(abortCondition, times(4)).call(response);
        verify(onSuccess, times(1)).accept(response);
    }

    @Test(description = "Testing the retry and fail by abort behavior of the CompletableFuture returned by the service")
    public void testGetAsyncResponse_retryFailAbort() throws Throwable {
        CompletableFuture<AutodeskResponse> future;
        AutodeskResponse response = null;

        when(callable.call()).thenReturn(response);
        when(doneCondition.call(response)).thenReturn(false, false, false, false);
        when(abortCondition.call(response)).thenReturn(false, false, false, true);

        future = service.getAsyncResponse(callable, onSuccess, doneCondition, abortCondition);
        response = future.get();

        verify(doneCondition, times(5)).call(response);
        verify(abortCondition, times(4)).call(response);
        verify(onSuccess, times(0)).accept(response);
    }

    @Test(description = "Testing the retry and fail by max retries behavior of the CompletableFuture returned by the service")
    public void testGetAsyncResponse_retryFailRetries() throws Throwable {
        CompletableFuture<AutodeskResponse> future;
        AutodeskResponse response = null;

        when(callable.call()).thenReturn(response);
        when(doneCondition.call(response)).thenReturn(false);
        when(abortCondition.call(response)).thenReturn(false);

        future = service.getAsyncResponse(callable, onSuccess, doneCondition, abortCondition);
        response = future.get();

        verify(doneCondition, times(MAX_RETRIES + 2)).call(response);
        verify(abortCondition, times(MAX_RETRIES + 1)).call(response);
        verify(onSuccess, times(0)).accept(response);
    }

    @Test(description = "Testing the success behavior of the Failsafe returned by the service")
    public void testGetWithRetryCondition_success() throws Throwable {
        AutodeskResponse response = null;
        Exception exception = new Exception();

        when(callable.call()).thenReturn(response);
        when(retryCondition.call(exception)).thenReturn(false);

        service.getWithRetryCondition(callable, retryCondition);

        verify(retryCondition, times(0)).call(exception);
    }

    @Test(description = "Testing the retry success behavior of the Failsafe returned by the service")
    public void testGetWithRetryCondition_retrySuccess() throws Throwable {
        AutodeskResponse response = null;
        Exception exception = new Exception();

        when(callable.call()).thenThrow(exception).thenReturn(response);
        when(retryCondition.call(exception)).thenReturn(true);

        service.getWithRetryCondition(callable, retryCondition);

        verify(retryCondition, times(1)).call(exception);
    }

    @Test(expectedExceptions = AutodeskRetryServiceException.class, description = "Testing the retry fail behavior of the Failsafe returned by the service")
    public void testGetWithRetryCondition_retryFail() throws Throwable {
        AutodeskResponse response = null;
        Exception exception = new Exception();

        try {
            when(callable.call()).thenThrow(exception).thenReturn(response);
            when(retryCondition.call(exception)).thenReturn(false);

            service.getWithRetryCondition(callable, retryCondition);
            Assertions.fail("Exception should be thrown");
        } finally {
            verify(retryCondition, times(1)).call(exception);
        }
    }

    @Test(expectedExceptions = AutodeskRetryServiceException.class, description = "Testing the retry fail by max retries exceed behavior of the Failsafe returned by the service")
    public void testGetWithRetryCondition_retryMaxRetry() throws Throwable {
        Exception exception = new Exception();

        try {
            when(callable.call()).thenThrow(exception);
            when(retryCondition.call(exception)).thenReturn(true);

            service.getWithRetryCondition(callable, retryCondition);
            Assertions.fail("Exception should be thrown");
        } finally {
            verify(retryCondition, times(MAX_RETRIES + 1)).call(exception);
        }
    }

}
