package com.appdirect.connector.autodesk.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.appdirect.connector.autodesk.entity.AutodeskOrder;
import com.appdirect.connector.autodesk.exception.AutodeskAPIException;
import com.appdirect.connector.autodesk.exception.AutodeskResponseConvertException;
import com.appdirect.connector.autodesk.exception.AutodeskRetryServiceException;
import com.appdirect.connector.autodesk.exception.AutodeskServiceException;
import com.appdirect.connector.autodesk.model.AutodeskProduct;
import com.appdirect.connector.autodesk.respository.AutodeskOrderRepository;
import com.appdirect.connector.autodesk.rest.converter.DefaultConverter;
import com.appdirect.connector.autodesk.rest.converter.OrderDetailsConverter;
import com.appdirect.connector.autodesk.rest.model.AutodeskResponse;
import com.appdirect.connector.autodesk.rest.model.order.cancel.CancelOrderRequest;
import com.appdirect.connector.autodesk.rest.model.order.common.RevokeResponse;
import com.appdirect.connector.autodesk.rest.model.order.details.Element;
import com.appdirect.connector.autodesk.rest.model.order.details.Message;
import com.appdirect.connector.autodesk.rest.model.order.details.OrderDetailsResponse;
import com.appdirect.connector.autodesk.rest.model.order.details.OrderHeaderArray;
import com.appdirect.connector.autodesk.rest.model.order.details.OrderItemsArray;
import com.appdirect.connector.autodesk.rest.model.order.place.PlaceOrderRequest;
import com.appdirect.connector.autodesk.rest.model.order.place.PlaceOrderResponse;
import com.appdirect.connector.autodesk.rest.model.order.status.OrderStatusResponse;
import com.appdirect.connector.autodesk.rest.model.products.add.AddProductsRequest;
import com.appdirect.connector.autodesk.rest.model.products.add.AddProductsResponse;
import com.appdirect.connector.autodesk.rest.model.products.price.Price;
import com.appdirect.connector.autodesk.rest.model.products.price.PriceResponse;
import com.appdirect.connector.autodesk.rest.model.products.remove.RemoveProductsRequest;
import com.appdirect.connector.autodesk.rest.model.seats.add.AddSeatsRequest;
import com.appdirect.connector.autodesk.rest.model.seats.add.AddSeatsResponse;
import com.appdirect.connector.autodesk.rest.model.seats.remove.RemoveSeatsRequest;
import com.appdirect.connector.autodesk.type.OperationType;
import com.appdirect.connector.autodesk.type.StatusType;
import com.appdirect.connector.autodesk.util.DateProvider;
import com.appdirect.connector.autodesk.util.RequestBuilderHelper;

import retrofit2.Call;
import retrofit2.Response;

public class AutodeskServiceImplTest {

	private static final String CONTRACT_NUMBER = "contractNumber";
	private static final String STATUS = "OK";
	private static final String EXCEPTION_MESSAGE = "Exception message";
	private static final String MOCK_REFERENCE = "mockReference";
	private static final String PO_NUMBER = "po";
    private static final String SKU = "sku";
    private static final String PRODUCT_NAME = "product";
	private static final String SUBSCRIPTION_ID = "subscription";
	private static final Double PRICE = 10.0d;

	@Mock
    private AutodeskRestService restService;

    @Mock
    private RequestBuilderHelper requestBuilderHelper;

    @Mock
    private DefaultConverter defaultConverter;

    @Mock
    private OrderDetailsConverter orderDetailsConverter;

    @Mock
    private Call<PlaceOrderResponse> placeOrderCall;

    @Mock
    private Call<OrderStatusResponse> orderStatusCall;

    @Mock
    private Call<OrderDetailsResponse> orderDetailsCall;

    @Mock
    private Call<AddSeatsResponse> addSeatsCall;

    @Mock
    private Call<AddProductsResponse> addProductsCall;

    @Mock
    private Call<RevokeResponse> removeSeatsCall;

    @Mock
    private Call<RevokeResponse> removeProductsCall;

    @Mock
    private Call<RevokeResponse> cancelOrderCall;

    @Mock
    CompletableFuture<AutodeskResponse> futureMock;

    @Mock
    private AutodeskServiceImpl mockService;

    @Mock
    private AutodeskRetryService mockRetryService;

    @Mock
    private DateProvider dateProvider;

    @Mock
    private AutodeskProductService productService;

    @Mock
    private AutodeskProduct product;

    @Mock
    private PriceResponse priceResponse;

    @Mock
    private Price price;

    @Mock
    private AutodeskOrderRepository orderRepository;

    @Mock
    private AutodeskRetryService retryService;

    @InjectMocks
    @Spy
    private AutodeskServiceImpl service;


    @BeforeMethod
    public void setUp() throws AutodeskServiceException, AutodeskAPIException {
        MockitoAnnotations.initMocks(this);
    }

	@Test(description = "Testing the success behavior of the placeOrder functionality in the service")
    public void testPlaceOrder_success() throws Throwable {
        PlaceOrderRequest request = PlaceOrderRequest.builder().build();
        PlaceOrderResponse result = Mockito.mock(PlaceOrderResponse.class);
        Mockito.when(result.getStatus()).thenReturn(StatusType.ACCEPTED);
        Mockito.when(result.getReferenceNumber()).thenReturn(MOCK_REFERENCE);

        setup_placeOrder(request, result);

        this.service.placeOrder(PO_NUMBER, PRODUCT_NAME, 1);

        assertThat(result.getStatus()).isEqualTo(StatusType.ACCEPTED);
        verify(service).asyncFutureWithCallback(eq(MOCK_REFERENCE), isA(Consumer.class));
    }

    @Test(expectedExceptions = AutodeskServiceException.class, description = "Testing the fail behavior of the placeOrder functionality when a IOException is generated")
    public void testPlaceOrder_IOException() throws Throwable {
        PlaceOrderRequest request = PlaceOrderRequest.builder().build();

        setup_placeOrder_IOException(request);
        this.service.placeOrder(PO_NUMBER, PRODUCT_NAME, 1);
    }

    @Test(expectedExceptions = AutodeskAPIException.class, description = "Testing the fail behavior of the placeOrder functionality when a AutodeskAPIException is generated")
    public void testPlaceOrder_APIException() throws Throwable {
        PlaceOrderRequest request = PlaceOrderRequest.builder().build();
        AutodeskAPIException exception = new AutodeskAPIException(1, STATUS, EXCEPTION_MESSAGE);

        setup_placeOrder_AutodeskException(request, exception);
        this.service.placeOrder(PO_NUMBER, PRODUCT_NAME, 1);
    }

    @Test(expectedExceptions = AutodeskServiceException.class, description = "Testing the fail behavior of the placeOrder functionality when a AutodeskResponseConvertException is generated")
    public void testPlaceOrder_ResponseConvertException() throws Throwable {
        PlaceOrderRequest request = PlaceOrderRequest.builder().build();
        AutodeskResponseConvertException exception = new AutodeskResponseConvertException(new Exception(EXCEPTION_MESSAGE));

        setup_placeOrder_AutodeskException(request, exception);
        this.service.placeOrder(PO_NUMBER, PRODUCT_NAME, 1);
    }

    @Test(description = "Testing the success behavior of the getOrderStatus functionality in the service")
    public void testGetOrderStatus_success() throws Throwable {
    	String message = "Accepted";
        OrderStatusResponse result = new OrderStatusResponse();
        result.setStatus(StatusType.ACCEPTED);
        result.setLastUpdate(new Date());
        result.setMessage(message);

        setup_getOrderStatus(result);
        result = this.service.getOrderStatus(PO_NUMBER);
        assertThat(result.getStatus()).isEqualTo(StatusType.ACCEPTED);
        assertThat(result.getMessage()).isEqualTo(message);
        assertThat(result.getLastUpdate()).isToday();
    }

    @Test(expectedExceptions = AutodeskServiceException.class, description = "Testing the fail behavior of the getOrderStatus functionality when a IOException is generated")
    public void testGetOrderStatus_IOException() throws Exception {

        setup_getOrderStatus_IOException();
        this.service.getOrderStatus(PO_NUMBER);
    }

    @Test(expectedExceptions = AutodeskAPIException.class, description = "Testing the fail behavior of the getOrderStatus functionality when a AutodeskAPIException is generated")
    public void testGetOrderStatus_APIException() throws Throwable {
        AutodeskAPIException exception = new AutodeskAPIException(1, STATUS, EXCEPTION_MESSAGE);

        setup_getOrderStatus_AutodeskException(exception);
        this.service.getOrderStatus(PO_NUMBER);
    }

    @Test(expectedExceptions = AutodeskServiceException.class, description = "Testing the fail behavior of the getOrderStatus functionality when a AutodeskResponseConvertException is generated")
    public void testGetOrderStatus_ResponseConvertException() throws Throwable {
        AutodeskResponseConvertException exception = new AutodeskResponseConvertException(new Exception(EXCEPTION_MESSAGE));

        setup_getOrderStatus_AutodeskException(exception);
        this.service.getOrderStatus(PO_NUMBER);
    }

    @Test(description = "Testing the success behavior of the getOrderDetails functionality in the service")
    public void testGetOrderDetails_success() throws Throwable {
        OrderDetailsResponse result = new OrderDetailsResponse();
        result.setStatus(StatusType.ACCEPTED);

        setup_getOrderDetails(result);
        result = this.service.getOrderDetails(PO_NUMBER);
        assertThat(result.getStatus()).isEqualTo(StatusType.ACCEPTED);
    }

    @Test(expectedExceptions = AutodeskServiceException.class, description = "Testing the fail behavior of the getOrderDetails functionality when a IOException is generated")
    public void testGetOrderDetails_IOException() throws Exception {
        setup_getOrderDetails_IOException();
        this.service.getOrderDetails(PO_NUMBER);
    }

    @Test(expectedExceptions = AutodeskAPIException.class, description = "Testing the fail behavior of the getOrderDetails functionality when a AutodeskAPIException is generated")
    public void testGetOrderDetails_APIException() throws Throwable {
        AutodeskAPIException exception = new AutodeskAPIException(1, STATUS, EXCEPTION_MESSAGE);

        setup_getOrderDetails_AutodeskException(exception);
        this.service.getOrderDetails(PO_NUMBER);
    }

    @Test(expectedExceptions = AutodeskServiceException.class, description = "Testing the fail behavior of the getOrderDetails functionality when a AutodeskResponseConvertException is generated")
    public void testGetOrderDetails_ResponseConvertException() throws Throwable {
        AutodeskResponseConvertException exception = new AutodeskResponseConvertException(new Exception(EXCEPTION_MESSAGE));

        setup_getOrderDetails_AutodeskException(exception);
        this.service.getOrderDetails(PO_NUMBER);
    }

    @Test(description = "Testing the success behavior of the AddSeats functionality in the service")
    public void testAddSeats_success() throws Throwable {
        AddSeatsRequest request = AddSeatsRequest.builder().build();
        AddSeatsResponse result = new AddSeatsResponse();
        result.setMessage("Ok");
        result.setStatus(StatusType.ACCEPTED);
        result.setReferenceNumber(MOCK_REFERENCE);

        setup_addSeats(request, result);
        this.service.addSeats(PO_NUMBER, PRODUCT_NAME, 1);

        assertThat(result.getStatus()).isEqualTo(StatusType.ACCEPTED);
        assertThat(result.getMessage()).isNotEmpty();
        assertThat(result.getReferenceNumber()).isNotEmpty();
        verify(service).asyncFuture(MOCK_REFERENCE);
    }

    @Test(expectedExceptions = AutodeskServiceException.class, description = "Testing the fail behavior of the AddSeats functionality when a IOException is generated")
    public void testAddSeats_IOException() throws Throwable {
        AddSeatsRequest request = AddSeatsRequest.builder().build();

        setup_addSeats_IOException(request);
        this.service.addSeats(PO_NUMBER, PRODUCT_NAME, 1);
    }

    @Test(expectedExceptions = AutodeskAPIException.class, description = "Testing the fail behavior of the AddSeats functionality when a AutodeskAPIException is generated")
    public void testAddSeats_APIException() throws Throwable {
        AddSeatsRequest request = AddSeatsRequest.builder().build();
        AutodeskAPIException exception = new AutodeskAPIException(1, STATUS, EXCEPTION_MESSAGE);

        setup_addSeats_AutodeskException(request, exception);
        this.service.addSeats(PO_NUMBER, PRODUCT_NAME, 1);
    }

    @Test(expectedExceptions = AutodeskServiceException.class, description = "Testing the fail behavior of the AddSeats functionality when a AutodeskResponseConvertException is generated")
    public void testAddSeats_ResponseConvertException() throws Throwable {
        AddSeatsRequest request = AddSeatsRequest.builder().build();
        AutodeskResponseConvertException exception = new AutodeskResponseConvertException(new Exception(EXCEPTION_MESSAGE));

        setup_addSeats_AutodeskException(request, exception);
        this.service.addSeats(PO_NUMBER, PRODUCT_NAME, 1);
    }

    @Test(description = "Testing the success behavior of the AddProducts functionality in the service")
    public void testAddProducts_success() throws Throwable {
        AddProductsRequest request = AddProductsRequest.builder().build();
        AddProductsResponse result = new AddProductsResponse();
        result.setStatus(StatusType.ACCEPTED);
        result.setReferenceNumber(MOCK_REFERENCE);

        setup_addProducts(request, result);
        this.service.addProducts(PO_NUMBER, PRODUCT_NAME, 1);

        assertThat(result.getStatus()).isEqualTo(StatusType.ACCEPTED);
        verify(service).asyncFuture(MOCK_REFERENCE);
    }

    @Test(expectedExceptions = AutodeskServiceException.class, description = "Testing the fail behavior of the AddProducts functionality when a IOException is generated")
    public void testAddProducts_IOException() throws Throwable {
        AddProductsRequest request = AddProductsRequest.builder().build();

        setup_addProducts_IOException(request);
        this.service.addProducts(PO_NUMBER, PRODUCT_NAME, 1);
    }

    @Test(expectedExceptions = AutodeskAPIException.class, description = "Testing the fail behavior of the AddProducts functionality when a AutodeskAPIException is generated")
    public void testAddProducts_APIException() throws Throwable {
        AddProductsRequest request = AddProductsRequest.builder().build();
        AutodeskAPIException exception = new AutodeskAPIException(1, STATUS, EXCEPTION_MESSAGE);

        setup_addProducts_AutodeskException(request, exception);
        this.service.addProducts(PO_NUMBER, PRODUCT_NAME, 1);
    }

    @Test(expectedExceptions = AutodeskServiceException.class, description = "Testing the fail behavior of the AddProducts functionality when a AutodeskResponseConvertException is generated")
    public void testAddProducts_ResponseConvertException() throws Throwable {
        AddProductsRequest request = AddProductsRequest.builder().build();
        AutodeskResponseConvertException exception = new AutodeskResponseConvertException(new Exception(EXCEPTION_MESSAGE));

        setup_addProducts_AutodeskException(request, exception);
        this.service.addProducts(PO_NUMBER, PRODUCT_NAME, 1);
    }

    @Test(description = "Testing the success behavior of the RemoveSeats functionality in the service")
    public void testRemoveSeats_success() throws Throwable {
        RemoveSeatsRequest request = RemoveSeatsRequest.builder().build();
        RevokeResponse result = new RevokeResponse();
        result.setStatus(StatusType.ACCEPTED);
        result.setReferenceNumber(MOCK_REFERENCE);

        setup_removeSeats(1, request, result);
        this.service.removeSeats(PO_NUMBER, PRODUCT_NAME, 1);

        assertThat(result.getStatus()).isEqualTo(StatusType.ACCEPTED);
        verify(service).asyncFuture(MOCK_REFERENCE);
    }

    @Test(expectedExceptions = AutodeskServiceException.class, description = "Testing the fail behavior of the RemoveSeats functionality when a IOException is generated")
    public void testRemoveSeats_IOException() throws Throwable {
        RemoveSeatsRequest request = RemoveSeatsRequest.builder().build();

        setup_removeSeats_IOException(1, request);
        this.service.removeSeats(PO_NUMBER, PRODUCT_NAME, 1);
    }

    @Test(expectedExceptions = AutodeskAPIException.class, description = "Testing the fail behavior of the RemoveSeats functionality when a AutodeskAPIException is generated")
    public void testRemoveSeats_APIException() throws Throwable {
        RemoveSeatsRequest request = RemoveSeatsRequest.builder().build();
        AutodeskAPIException exception = new AutodeskAPIException(1, STATUS, EXCEPTION_MESSAGE);

        setup_removeSeats_AutodeskException(1, request, exception);
        this.service.removeSeats(PO_NUMBER, PRODUCT_NAME, 1);
    }

    @Test(expectedExceptions = AutodeskServiceException.class, description = "Testing the fail behavior of the RemoveSeats functionality when a AutodeskResponseConvertException is generated")
    public void testRemoveSeats_ResponseConvertException() throws Throwable {
        RemoveSeatsRequest request = RemoveSeatsRequest.builder().build();
        AutodeskResponseConvertException exception = new AutodeskResponseConvertException(new Exception(EXCEPTION_MESSAGE));

        setup_removeSeats_AutodeskException(1, request, exception);
        this.service.removeSeats(PO_NUMBER, PRODUCT_NAME, 1);
    }

    @Test(description = "Testing the success behavior of the RemoveProducts functionality in the service")
    public void testRemoveProducts_success() throws Throwable {
        RemoveProductsRequest request = RemoveProductsRequest.builder().build();
        RevokeResponse result = new RevokeResponse();
        result.setStatus(StatusType.ACCEPTED);
        result.setReferenceNumber(MOCK_REFERENCE);

        setup_removeProducts(request, result);
        this.service.removeProducts(PO_NUMBER, PRODUCT_NAME);

        assertThat(result.getStatus()).isEqualTo(StatusType.ACCEPTED);
        verify(service).asyncFuture(MOCK_REFERENCE);
    }

    @Test(expectedExceptions = AutodeskServiceException.class, description = "Testing the fail behavior of the RemoveProducts functionality when a IOException is generated")
    public void testRemoveProducts_IOException() throws Throwable {
        RemoveProductsRequest request = RemoveProductsRequest.builder().build();

        setup_removeProducts_IOException(request);
        this.service.removeProducts(PO_NUMBER, PRODUCT_NAME);
    }

    @Test(expectedExceptions = AutodeskAPIException.class, description = "Testing the fail behavior of the RemoveProducts functionality when a AutodeskAPIException is generated")
    public void testRemoveProducts_APIException() throws Throwable {
        RemoveProductsRequest request = RemoveProductsRequest.builder().build();
        AutodeskAPIException exception = new AutodeskAPIException(1, STATUS, EXCEPTION_MESSAGE);

        setup_removeProducts_AutodeskException(request, exception);
        this.service.removeProducts(PO_NUMBER, PRODUCT_NAME);
    }

    @Test(expectedExceptions = AutodeskServiceException.class, description = "Testing the fail behavior of the RemoveProducts functionality when a AutodeskResponseConvertException is generated")
    public void testRemoveProducts_ResponseConvertException() throws Throwable {
        RemoveProductsRequest request = RemoveProductsRequest.builder().build();
        AutodeskResponseConvertException exception = new AutodeskResponseConvertException(new Exception(EXCEPTION_MESSAGE));

        setup_removeProducts_AutodeskException(request, exception);
        this.service.removeProducts(PO_NUMBER, PRODUCT_NAME);
    }

    @Test(description = "Testing the success behavior of the cancelOrder functionality in the service")
    public void testCancelOrder_success() throws Throwable {
        CancelOrderRequest request = CancelOrderRequest.builder().build();
        RevokeResponse result = new RevokeResponse();
        result.setStatus(StatusType.ACCEPTED);
        result.setReferenceNumber(MOCK_REFERENCE);

        setup_cancelOrder(request, result);
        this.service.cancelOrder(PO_NUMBER);

        assertThat(result.getStatus()).isEqualTo(StatusType.ACCEPTED);
        verify(service).asyncFuture(MOCK_REFERENCE);
    }

    @Test(expectedExceptions = AutodeskServiceException.class, description = "Testing the fail behavior of the cancelOrder functionality when a IOException is generated")
    public void testCancelOrder_IOException() throws Throwable {
        CancelOrderRequest request = CancelOrderRequest.builder().build();

        setup_cancelOrder_IOException(request);
        this.service.cancelOrder(PO_NUMBER);
    }

    @Test(expectedExceptions = AutodeskAPIException.class, description = "Testing the fail behavior of the cancelOrder functionality when a AutodeskAPIException is generated")
    public void testCancelOrder_APIException() throws Throwable {
        CancelOrderRequest request = CancelOrderRequest.builder().build();
        AutodeskAPIException exception = new AutodeskAPIException(1, STATUS, EXCEPTION_MESSAGE);

        setup_cancelOrder_AutodeskException(request, exception);
        this.service.cancelOrder(PO_NUMBER);
    }

    @Test(expectedExceptions = AutodeskServiceException.class, description = "Testing the fail behavior of the cancelOrder functionality when a AutodeskResponseConvertException is generated")
    public void testCancelOrder_ResponseConvertException() throws Throwable {
        CancelOrderRequest request = CancelOrderRequest.builder().build();
        AutodeskResponseConvertException exception = new AutodeskResponseConvertException(new Exception(EXCEPTION_MESSAGE));

        setup_cancelOrder_AutodeskException(request, exception);
        this.service.cancelOrder(PO_NUMBER);
    }

    @Test
	public void testGetContractNumber_successFromRepository() throws AutodeskServiceException, AutodeskAPIException {
		AutodeskOrder autodeskOrder = Mockito.mock(AutodeskOrder.class);

		when(orderRepository.findOne(PO_NUMBER)).thenReturn(autodeskOrder);
		when(autodeskOrder.getContractNumber()).thenReturn(CONTRACT_NUMBER);

		String contractNumber = this.service.getContractNumber(PO_NUMBER);

		assertThat(contractNumber).isEqualTo(CONTRACT_NUMBER);
	}

	@Test
	public void testGetContractNumber_successFromService() throws AutodeskServiceException, AutodeskAPIException {
		AutodeskOrder autodeskOrder = Mockito.mock(AutodeskOrder.class);
		OrderDetailsResponse orderDetailsResponse = Mockito.mock(OrderDetailsResponse.class);

		when(orderRepository.findOne(PO_NUMBER)).thenReturn(autodeskOrder);
		when(autodeskOrder.getContractNumber()).thenReturn(null);
		when(orderDetailsResponse.getContractNumber()).thenReturn(CONTRACT_NUMBER);
		doReturn(orderDetailsResponse).when(service).getOrderDetails(PO_NUMBER);

		String contractNumber = this.service.getContractNumber(PO_NUMBER);

		assertThat(contractNumber).isEqualTo(CONTRACT_NUMBER);
	}

	@Test(expectedExceptions = AutodeskAPIException.class)
	public void testGetContractNumber_failure() throws AutodeskServiceException, AutodeskAPIException {
		AutodeskOrder autodeskOrder = Mockito.mock(AutodeskOrder.class);

		when(orderRepository.findOne(PO_NUMBER)).thenReturn(autodeskOrder);
		when(autodeskOrder.getContractNumber()).thenReturn(null);
		Mockito.doThrow(new AutodeskAPIException(400, "Error", "Error message")).when(service).getOrderDetails(PO_NUMBER);

		String contractNumber = this.service.getContractNumber(PO_NUMBER);

		assertThat(contractNumber).isEqualTo(CONTRACT_NUMBER);
	}

	@Test(description = "Tests get subscription id is success")
	public void testGetSubscriptionId_success() throws AutodeskServiceException, AutodeskAPIException {
		AutodeskProduct product = Mockito.mock(AutodeskProduct.class);
		OrderDetailsResponse orderDetailsResponse = Mockito.mock(OrderDetailsResponse.class);
		Message message = Mockito.mock(Message.class);
		Element element = Mockito.mock(Element.class);
		OrderHeaderArray header = Mockito.mock(OrderHeaderArray.class);
		OrderItemsArray item = Mockito.mock(OrderItemsArray.class);

		when(item.getSubsId()).thenReturn(SUBSCRIPTION_ID);
		when(item.getSku()).thenReturn(SKU);
		when(item.getSalesLicenseType()).thenReturn("New");
		when(element.getOrderItemsArray()).thenReturn(Arrays.asList(item));
		when(header.getContractNumber()).thenReturn(CONTRACT_NUMBER);
		when(element.getOrderHeaderArray()).thenReturn(Arrays.asList(header));
		when(message.getElements()).thenReturn(Arrays.asList(element));
		when(orderDetailsResponse.getMessage()).thenReturn(message);
		when(product.getSku()).thenReturn(SKU);
		when(productService.getProductByNameAndOperation(PRODUCT_NAME, OperationType.INITIAL_ORDER)).thenReturn(Optional.ofNullable(product));
		doReturn(orderDetailsResponse).when(service).getOrderDetails(PO_NUMBER);
		String subscriptionId = service.getSubscriptionId(PO_NUMBER, CONTRACT_NUMBER, PRODUCT_NAME);

		assertThat(subscriptionId).isEqualTo(SUBSCRIPTION_ID);
	}

	@Test(expectedExceptions=AutodeskServiceException.class, description = "Tests get subscription id fails because no items with expected sku")
	public void testGetSubscriptionId_noItemWithSku() throws AutodeskServiceException, AutodeskAPIException {
		AutodeskProduct product = Mockito.mock(AutodeskProduct.class);
		OrderDetailsResponse orderDetailsResponse = Mockito.mock(OrderDetailsResponse.class);
		Message message = Mockito.mock(Message.class);
		Element element = Mockito.mock(Element.class);
		OrderHeaderArray header  = Mockito.mock(OrderHeaderArray.class);

		when(header.getContractNumber()).thenReturn(CONTRACT_NUMBER);
		when(element.getOrderHeaderArray()).thenReturn(Arrays.asList(header));
		when(message.getElements()).thenReturn(Arrays.asList(element));
		when(orderDetailsResponse.getMessage()).thenReturn(message);
		when(product.getSku()).thenReturn(SKU);
		when(productService.getProductByNameAndOperation(PRODUCT_NAME, OperationType.INITIAL_ORDER)).thenReturn(Optional.ofNullable(product));
		doReturn(orderDetailsResponse).when(service).getOrderDetails(PO_NUMBER);
		service.getSubscriptionId(PO_NUMBER, CONTRACT_NUMBER, PRODUCT_NAME);
	}

	@Test(expectedExceptions=AutodeskServiceException.class, description = "Tests get subscription id fails because no sku is found for product name")
	public void testGetSubscriptionId_noSKU() throws AutodeskServiceException, AutodeskAPIException {
		AutodeskProduct product = null;
		when(productService.getProductByNameAndOperation(PRODUCT_NAME, OperationType.INITIAL_ORDER)).thenReturn(Optional.ofNullable(product));
		service.getSubscriptionId(PO_NUMBER, CONTRACT_NUMBER, PRODUCT_NAME);
	}

    @Test
    public void testSaveOrder_success() throws AutodeskServiceException, AutodeskAPIException, AutodeskRetryServiceException {
        when(retryService.getWithRetryCondition(isA(Callable.class), isA(FailureCallable.class))).thenReturn(CONTRACT_NUMBER);

        this.service.saveOrder(PO_NUMBER);

        verify(orderRepository, times(1)).save(isA(AutodeskOrder.class));
    }

    @Test(description = "Tests get subscription id fails because no sku is found for product name")
    public void testSaveOrder_AutodeskServiceException() throws AutodeskServiceException, AutodeskAPIException, AutodeskRetryServiceException {
        when(retryService.getWithRetryCondition(isA(Callable.class), isA(FailureCallable.class))).thenThrow(new AutodeskRetryServiceException("", new Exception()));

        this.service.saveOrder(PO_NUMBER);
        verify(orderRepository, times(0)).save(isA(AutodeskOrder.class));
    }

    private void setup_placeOrder(PlaceOrderRequest request, PlaceOrderResponse result) throws Throwable {
        Response<PlaceOrderResponse> response = null;

        doReturn(futureMock).when(service).asyncFuture(MOCK_REFERENCE);
        when(productService.getProductByNameAndOperation(PRODUCT_NAME, OperationType.INITIAL_ORDER)).thenReturn(Optional.of(product));
        when(product.getSku()).thenReturn(SKU);
        when(requestBuilderHelper.buildPlaceOrderRequest(PO_NUMBER, SKU, 1, 10.0d)).thenReturn(request);
        when(restService.placeOrder(null, request)).thenReturn(placeOrderCall);
        when(placeOrderCall.execute()).thenReturn(response);
        when(defaultConverter.process(response)).thenReturn(result);
        when(productService.getPrice(SKU)).thenReturn(priceResponse);
        when(priceResponse.getResponse()).thenReturn(price);
        when(price.getNetPrice()).thenReturn(PRICE);
    }

    private void setup_placeOrder_IOException(PlaceOrderRequest request) throws Throwable {
        IOException exception = new IOException(EXCEPTION_MESSAGE);

        when(productService.getProductByNameAndOperation(PRODUCT_NAME, OperationType.INITIAL_ORDER)).thenReturn(Optional.of(product));
        when(product.getSku()).thenReturn(SKU);
        when(requestBuilderHelper.buildPlaceOrderRequest(PO_NUMBER, SKU, 1, 10.0d)).thenReturn(request);
        when(restService.placeOrder(null, request)).thenReturn(placeOrderCall);
        when(placeOrderCall.execute()).thenThrow(exception);
        when(productService.getPrice(SKU)).thenReturn(priceResponse);
        when(priceResponse.getResponse()).thenReturn(price);
        when(price.getNetPrice()).thenReturn(PRICE);
    }

    private void setup_placeOrder_AutodeskException(PlaceOrderRequest request, Exception exception) throws Throwable {
        Response<PlaceOrderResponse> response = null;

        when(productService.getProductByNameAndOperation(PRODUCT_NAME, OperationType.INITIAL_ORDER)).thenReturn(Optional.of(product));
        when(product.getSku()).thenReturn(SKU);
        when(requestBuilderHelper.buildPlaceOrderRequest(PO_NUMBER, SKU, 1, 10.0d)).thenReturn(request);
        when(restService.placeOrder(null, request)).thenReturn(placeOrderCall);
        when(placeOrderCall.execute()).thenReturn(response);
        when(defaultConverter.process(response)).thenThrow(exception);
        when(productService.getPrice(SKU)).thenReturn(priceResponse);
        when(priceResponse.getResponse()).thenReturn(price);
        when(price.getNetPrice()).thenReturn(PRICE);
    }

    private void setup_getOrderStatus(OrderStatusResponse result) throws Throwable {
        Response<OrderStatusResponse> response = Response.success(new OrderStatusResponse());

        when(restService.getOrderStatus(null, PO_NUMBER)).thenReturn(orderStatusCall);
        when(orderStatusCall.execute()).thenReturn(response);
        when(defaultConverter.process(response)).thenReturn(result);
    }

    private void setup_getOrderStatus_IOException() throws IOException {
        IOException exception = new IOException(EXCEPTION_MESSAGE);

        when(restService.getOrderStatus(null, PO_NUMBER)).thenReturn(orderStatusCall);
        when(orderStatusCall.execute()).thenThrow(exception);
    }

    private void setup_getOrderStatus_AutodeskException(Exception exception) throws Throwable {
        Response<OrderStatusResponse> response = null;

        when(restService.getOrderStatus(null, PO_NUMBER)).thenReturn(orderStatusCall);
        when(orderStatusCall.execute()).thenReturn(response);
        when(defaultConverter.process(response)).thenThrow(exception);
    }

    private void setup_getOrderDetails(OrderDetailsResponse result) throws Throwable {
        Response<OrderDetailsResponse> response = null;

        when(restService.getOrderDetails(null, null, PO_NUMBER)).thenReturn(orderDetailsCall);
        when(orderDetailsCall.execute()).thenReturn(response);
        when(orderDetailsConverter.process(response)).thenReturn(result);
    }

    private void setup_getOrderDetails_IOException() throws IOException {
        IOException exception = new IOException(EXCEPTION_MESSAGE);

        when(restService.getOrderDetails(null, null, PO_NUMBER)).thenReturn(orderDetailsCall);
        when(orderDetailsCall.execute()).thenThrow(exception);
    }

    private void setup_getOrderDetails_AutodeskException(Exception exception) throws Throwable {
        Response<OrderDetailsResponse> response = null;

        when(restService.getOrderDetails(null, null, PO_NUMBER)).thenReturn(orderDetailsCall);
        when(orderDetailsCall.execute()).thenReturn(response);
        when(orderDetailsConverter.process(response)).thenThrow(exception);
    }

    private void setup_addSeats(AddSeatsRequest request, AddSeatsResponse result) throws Throwable {
        Response<AddSeatsResponse> response = null;

        doReturn(CONTRACT_NUMBER).when(service).getContractNumber(PO_NUMBER);
        doReturn(SUBSCRIPTION_ID).when(service).getSubscriptionId(PO_NUMBER, CONTRACT_NUMBER, PRODUCT_NAME);
        when(productService.getProductByNameAndOperation(PRODUCT_NAME, OperationType.ADD_SEAT)).thenReturn(Optional.of(product));
        when(product.getSku()).thenReturn(SKU);
        when(requestBuilderHelper.buildAddSeatsRequest(PO_NUMBER, CONTRACT_NUMBER, SUBSCRIPTION_ID, SKU, 1, 10.0d)).thenReturn(request);
        when(restService.addSeats(null, request)).thenReturn(addSeatsCall);
        when(addSeatsCall.execute()).thenReturn(response);
        when(defaultConverter.process(response)).thenReturn(result);
        when(productService.getPrice(SKU)).thenReturn(priceResponse);
        when(priceResponse.getResponse()).thenReturn(price);
        when(price.getNetPrice()).thenReturn(PRICE);
    }

    private void setup_addSeats_IOException(AddSeatsRequest request) throws Throwable {
        IOException exception = new IOException(EXCEPTION_MESSAGE);

        doReturn(CONTRACT_NUMBER).when(service).getContractNumber(PO_NUMBER);
        doReturn(SUBSCRIPTION_ID).when(service).getSubscriptionId(PO_NUMBER, CONTRACT_NUMBER, PRODUCT_NAME);
        when(productService.getProductByNameAndOperation(PRODUCT_NAME, OperationType.ADD_SEAT)).thenReturn(Optional.of(product));
        when(product.getSku()).thenReturn(SKU);
        when(requestBuilderHelper.buildAddSeatsRequest(PO_NUMBER, CONTRACT_NUMBER, SUBSCRIPTION_ID, SKU, 1, 10.0d)).thenReturn(request);
        when(restService.addSeats(null, request)).thenReturn(addSeatsCall);
        when(addSeatsCall.execute()).thenThrow(exception);
        when(productService.getPrice(SKU)).thenReturn(priceResponse);
        when(priceResponse.getResponse()).thenReturn(price);
        when(price.getNetPrice()).thenReturn(PRICE);
    }

    private void setup_addSeats_AutodeskException(AddSeatsRequest request, Exception exception) throws Throwable {
        Response<AddSeatsResponse> response = null;

        doReturn(CONTRACT_NUMBER).when(service).getContractNumber(PO_NUMBER);
        doReturn(SUBSCRIPTION_ID).when(service).getSubscriptionId(PO_NUMBER, CONTRACT_NUMBER, PRODUCT_NAME);
        when(productService.getProductByNameAndOperation(PRODUCT_NAME, OperationType.ADD_SEAT)).thenReturn(Optional.of(product));
        when(product.getSku()).thenReturn(SKU);
        when(requestBuilderHelper.buildAddSeatsRequest(PO_NUMBER, CONTRACT_NUMBER, SUBSCRIPTION_ID, SKU, 1, 10.0d)).thenReturn(request);
        when(restService.addSeats(null, request)).thenReturn(addSeatsCall);
        when(addSeatsCall.execute()).thenReturn(response);
        when(defaultConverter.process(response)).thenThrow(exception);
        when(productService.getPrice(SKU)).thenReturn(priceResponse);
        when(priceResponse.getResponse()).thenReturn(price);
        when(price.getNetPrice()).thenReturn(PRICE);
    }

    private void setup_addProducts(AddProductsRequest request, AddProductsResponse result) throws Throwable {
        Response<AddProductsResponse> response = null;

        doReturn(CONTRACT_NUMBER).when(service).getContractNumber(PO_NUMBER);
        when(productService.getProductByNameAndOperation(PRODUCT_NAME, OperationType.ADD_PRODUCT)).thenReturn(Optional.of(product));
        when(product.getSku()).thenReturn(SKU);
        when(requestBuilderHelper.buildAddProductsRequest(PO_NUMBER, CONTRACT_NUMBER, SKU, 1, 10.0d)).thenReturn(request);
        when(restService.addProducts(null, request)).thenReturn(addProductsCall);
        when(addProductsCall.execute()).thenReturn(response);
        when(defaultConverter.process(response)).thenReturn(result);
        when(productService.getPrice(SKU)).thenReturn(priceResponse);
        when(priceResponse.getResponse()).thenReturn(price);
        when(price.getNetPrice()).thenReturn(PRICE);
    }

    private void setup_addProducts_IOException(AddProductsRequest request) throws Throwable {
        IOException exception = new IOException(EXCEPTION_MESSAGE);

        doReturn(CONTRACT_NUMBER).when(service).getContractNumber(PO_NUMBER);
        when(productService.getProductByNameAndOperation(PRODUCT_NAME, OperationType.ADD_PRODUCT)).thenReturn(Optional.of(product));
        when(product.getSku()).thenReturn(SKU);
        when(requestBuilderHelper.buildAddProductsRequest(PO_NUMBER, CONTRACT_NUMBER, SKU, 1, 10.0d)).thenReturn(request);
        when(restService.addProducts(null, request)).thenReturn(addProductsCall);
        when(addProductsCall.execute()).thenThrow(exception);
        when(productService.getPrice(SKU)).thenReturn(priceResponse);
        when(priceResponse.getResponse()).thenReturn(price);
        when(price.getNetPrice()).thenReturn(PRICE);
    }

    private void setup_addProducts_AutodeskException(AddProductsRequest request, Exception exception) throws Throwable {
        Response<AddProductsResponse> response = null;

        doReturn(CONTRACT_NUMBER).when(service).getContractNumber(PO_NUMBER);
        when(productService.getProductByNameAndOperation(PRODUCT_NAME, OperationType.ADD_PRODUCT)).thenReturn(Optional.of(product));
        when(product.getSku()).thenReturn(SKU);
        when(requestBuilderHelper.buildAddProductsRequest(PO_NUMBER, CONTRACT_NUMBER, SKU, 1, 10.0d)).thenReturn(request);
        when(restService.addProducts(null, request)).thenReturn(addProductsCall);
        when(addProductsCall.execute()).thenReturn(response);
        when(defaultConverter.process(response)).thenThrow(exception);
        when(productService.getPrice(SKU)).thenReturn(priceResponse);
        when(priceResponse.getResponse()).thenReturn(price);
        when(price.getNetPrice()).thenReturn(PRICE);
    }

    private void setup_removeSeats(int qty, RemoveSeatsRequest request, RevokeResponse result) throws Throwable {
        Response<RevokeResponse> response = null;

        doReturn(CONTRACT_NUMBER).when(service).getContractNumber(PO_NUMBER);
        doReturn(SUBSCRIPTION_ID).when(service).getSubscriptionId(PO_NUMBER, CONTRACT_NUMBER, PRODUCT_NAME);
        when(productService.getProductByNameAndOperation(PRODUCT_NAME, OperationType.REMOVE_SEAT)).thenReturn(Optional.of(product));
        when(product.getSku()).thenReturn(SKU);
        when(requestBuilderHelper.buildRemoveSeatsRequest(CONTRACT_NUMBER, SUBSCRIPTION_ID, qty)).thenReturn(request);
        when(restService.removeSeats(null, request)).thenReturn(removeSeatsCall);
        when(removeSeatsCall.execute()).thenReturn(response);
        when(defaultConverter.process(response)).thenReturn(result);
    }

    private void setup_removeSeats_IOException(int qty, RemoveSeatsRequest request) throws Throwable {
        IOException exception = new IOException(EXCEPTION_MESSAGE);

        doReturn(CONTRACT_NUMBER).when(service).getContractNumber(PO_NUMBER);
        doReturn(SUBSCRIPTION_ID).when(service).getSubscriptionId(PO_NUMBER, CONTRACT_NUMBER, PRODUCT_NAME);
        when(productService.getProductByNameAndOperation(PRODUCT_NAME, OperationType.REMOVE_SEAT)).thenReturn(Optional.of(product));
        when(product.getSku()).thenReturn(SKU);
        when(requestBuilderHelper.buildRemoveSeatsRequest(CONTRACT_NUMBER, SUBSCRIPTION_ID, qty)).thenReturn(request);
        when(restService.removeSeats(null, request)).thenReturn(removeSeatsCall);
        when(removeSeatsCall.execute()).thenThrow(exception);
    }

    private void setup_removeSeats_AutodeskException(int qty, RemoveSeatsRequest request, Exception exception) throws Throwable {
        Response<RevokeResponse> response = null;

        doReturn(CONTRACT_NUMBER).when(service).getContractNumber(PO_NUMBER);
        doReturn(SUBSCRIPTION_ID).when(service).getSubscriptionId(PO_NUMBER, CONTRACT_NUMBER, PRODUCT_NAME);
        when(productService.getProductByNameAndOperation(PRODUCT_NAME, OperationType.REMOVE_SEAT)).thenReturn(Optional.of(product));
        when(product.getSku()).thenReturn(SKU);
        when(requestBuilderHelper.buildRemoveSeatsRequest(CONTRACT_NUMBER, SUBSCRIPTION_ID, qty)).thenReturn(request);
        when(restService.removeSeats(null, request)).thenReturn(removeSeatsCall);
        when(removeSeatsCall.execute()).thenReturn(response);
        when(defaultConverter.process(response)).thenThrow(exception);
    }

    private void setup_removeProducts(RemoveProductsRequest request, RevokeResponse result) throws Throwable {
        Response<RevokeResponse> response = null;

        doReturn(CONTRACT_NUMBER).when(service).getContractNumber(PO_NUMBER);
        doReturn(SUBSCRIPTION_ID).when(service).getSubscriptionId(PO_NUMBER, CONTRACT_NUMBER, PRODUCT_NAME);
        when(productService.getProductByNameAndOperation(PRODUCT_NAME, OperationType.REMOVE_PRODUCT)).thenReturn(Optional.of(product));
        when(product.getSku()).thenReturn(SKU);
        when(requestBuilderHelper.buildRemoveProductsRequest(CONTRACT_NUMBER, SUBSCRIPTION_ID)).thenReturn(request);
        when(restService.removeProducts(null, request)).thenReturn(removeProductsCall);
        when(removeProductsCall.execute()).thenReturn(response);
        when(defaultConverter.process(response)).thenReturn(result);
    }

    private void setup_removeProducts_IOException(RemoveProductsRequest request) throws Throwable {
        IOException exception = new IOException(EXCEPTION_MESSAGE);

        doReturn(CONTRACT_NUMBER).when(service).getContractNumber(PO_NUMBER);
        doReturn(SUBSCRIPTION_ID).when(service).getSubscriptionId(PO_NUMBER, CONTRACT_NUMBER, PRODUCT_NAME);
        when(productService.getProductByNameAndOperation(PRODUCT_NAME, OperationType.REMOVE_PRODUCT)).thenReturn(Optional.of(product));
        when(product.getSku()).thenReturn(SKU);
        when(requestBuilderHelper.buildRemoveProductsRequest(CONTRACT_NUMBER, SUBSCRIPTION_ID)).thenReturn(request);
        when(restService.removeProducts(null, request)).thenReturn(removeProductsCall);
        when(removeProductsCall.execute()).thenThrow(exception);
    }

    private void setup_removeProducts_AutodeskException(RemoveProductsRequest request, Exception exception) throws Throwable {
        Response<RevokeResponse> response = null;

        doReturn(CONTRACT_NUMBER).when(service).getContractNumber(PO_NUMBER);
        doReturn(SUBSCRIPTION_ID).when(service).getSubscriptionId(PO_NUMBER, CONTRACT_NUMBER, PRODUCT_NAME);
        when(productService.getProductByNameAndOperation(PRODUCT_NAME, OperationType.REMOVE_PRODUCT)).thenReturn(Optional.of(product));
        when(product.getSku()).thenReturn(SKU);
        when(requestBuilderHelper.buildRemoveProductsRequest(CONTRACT_NUMBER, SUBSCRIPTION_ID)).thenReturn(request);
        when(restService.removeProducts(null, request)).thenReturn(removeProductsCall);
        when(removeProductsCall.execute()).thenReturn(response);
        when(defaultConverter.process(response)).thenThrow(exception);
    }

    private void setup_cancelOrder(CancelOrderRequest request, RevokeResponse result) throws Throwable {
        Response<RevokeResponse> response = null;

        doReturn(CONTRACT_NUMBER).when(service).getContractNumber(PO_NUMBER);
        when(requestBuilderHelper.buildCancelOrderRequest(CONTRACT_NUMBER)).thenReturn(request);
        when(restService.cancelOrder(null, request)).thenReturn(cancelOrderCall);
        when(cancelOrderCall.execute()).thenReturn(response);
        when(defaultConverter.process(response)).thenReturn(result);
    }

    private void setup_cancelOrder_IOException(CancelOrderRequest request) throws Throwable {
        IOException exception = new IOException(EXCEPTION_MESSAGE);

        doReturn(CONTRACT_NUMBER).when(service).getContractNumber(PO_NUMBER);
        when(requestBuilderHelper.buildCancelOrderRequest(CONTRACT_NUMBER)).thenReturn(request);
        when(restService.cancelOrder(null, request)).thenReturn(cancelOrderCall);
        when(cancelOrderCall.execute()).thenThrow(exception);
    }

    private void setup_cancelOrder_AutodeskException(CancelOrderRequest request, Exception exception) throws Throwable {
        Response<RevokeResponse> response = null;

        doReturn(CONTRACT_NUMBER).when(service).getContractNumber(PO_NUMBER);
        when(requestBuilderHelper.buildCancelOrderRequest(CONTRACT_NUMBER)).thenReturn(request);
        when(restService.cancelOrder(null, request)).thenReturn(cancelOrderCall);
        when(cancelOrderCall.execute()).thenReturn(response);
        when(defaultConverter.process(response)).thenThrow(exception);
    }

}
