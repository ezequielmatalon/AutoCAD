package com.appdirect.connector.autodesk.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.appdirect.connector.autodesk.rest.model.order.cancel.CancelOrderRequest;
import com.appdirect.connector.autodesk.rest.model.order.place.PlaceOrderRequest;
import com.appdirect.connector.autodesk.rest.model.products.add.AddProductsRequest;
import com.appdirect.connector.autodesk.rest.model.products.remove.RemoveProductsRequest;
import com.appdirect.connector.autodesk.rest.model.seats.add.AddSeatsRequest;
import com.appdirect.connector.autodesk.rest.model.seats.remove.RemoveSeatsRequest;
import com.appdirect.connector.autodesk.type.OperationType;

public class RequestBuilderHelperTest {

	@InjectMocks
	private RequestBuilderHelper helper;

	@BeforeMethod
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test(description = "Tests AddProductsRequest builder")
	public void testBuildAddProductsRequest() throws IOException {
		AddProductsRequest addProductsRequest = helper.buildAddProductsRequest("poNumber", "contractNumber", "sku", 2, 5.0d);
		assertThat(addProductsRequest.getActionName()).isEqualTo(OperationType.ADD_PRODUCT.getValue());
		assertThat(addProductsRequest.getContractNumber()).isEqualTo("contractNumber");
		assertThat(addProductsRequest.getPoNumber()).isEqualTo("poNumber");
		assertThat(addProductsRequest.getLineItems().get(0).getPartNumber()).isEqualTo("sku");
		assertThat(addProductsRequest.getLineItems().get(0).getQuantity()).isEqualTo(2);
		assertThat(addProductsRequest.getLineItems().get(0).getNetAmount()).isEqualTo("10.0");
	}

	@Test(description = "Tests AddSeatsRequest builder")
	public void testBuildAddSeatsRequest() throws IOException {
		AddSeatsRequest addSeatsRequest = helper.buildAddSeatsRequest("poNumber", "contractNumber", "subscriptionId", "sku", 2, 5.0d);
		assertThat(addSeatsRequest.getActionName()).isEqualTo(OperationType.ADD_SEAT.getValue());
		assertThat(addSeatsRequest.getContractNumber()).isEqualTo("contractNumber");
		assertThat(addSeatsRequest.getPoNumber()).isEqualTo("poNumber");
		assertThat(addSeatsRequest.getLineItems().get(0).getSubscriptionId()).isEqualTo("subscriptionId");
		assertThat(addSeatsRequest.getLineItems().get(0).getPartNumber()).isEqualTo("sku");
		assertThat(addSeatsRequest.getLineItems().get(0).getQuantity()).isEqualTo(2);
		assertThat(addSeatsRequest.getLineItems().get(0).getNetAmount()).isEqualTo("10.0");

	}

	@Test(description = "Tests PlaceOrderRequest builder")
	public void testBuildPlaceOrderRequest() throws IOException {
		PlaceOrderRequest placeOrderRequest = helper.buildPlaceOrderRequest("poNumber", "sku", 2, 5.0d);
		assertThat(placeOrderRequest.getActionName()).isEqualTo(OperationType.INITIAL_ORDER.getValue());
		assertThat(placeOrderRequest.getPoNumber()).isEqualTo("poNumber");
		assertThat(placeOrderRequest.getLineItems().get(0).getPartNumber()).isEqualTo("sku");
		assertThat(placeOrderRequest.getLineItems().get(0).getQuantity()).isEqualTo(2);
		assertThat(placeOrderRequest.getLineItems().get(0).getNetAmount()).isEqualTo("10.0");
	}

	@Test(description = "Tests CancelOrderRequest builder")
	public void testBuildCancelOrderRequest() throws IOException {
		CancelOrderRequest cancelOrderRequest = helper.buildCancelOrderRequest("contractNumber");
		assertThat(cancelOrderRequest.getActionName()).isEqualTo(OperationType.CANCEL_ORDER.getValue());
		assertThat(cancelOrderRequest.getContractNumber()).isEqualTo("contractNumber");
	}

	@Test(description = "Tests RemoveProductsRequest builder")
	public void testBuildRemoveProductsRequest() throws IOException {
		RemoveProductsRequest removeProductsRequest = helper.buildRemoveProductsRequest("contractNumber", "subscriptionId");
		assertThat(removeProductsRequest.getActionName()).isEqualTo(OperationType.REMOVE_PRODUCT.getValue());
		assertThat(removeProductsRequest.getContractNumber()).isEqualTo("contractNumber");
		assertThat(removeProductsRequest.getLineItems().get(0).getSubscriptionId()).isEqualTo("subscriptionId");
	}

	@Test(description = "Tests RemoveSeatsRequest builder")
	public void testBuildRemoveSeatsRequest() throws IOException {
		RemoveSeatsRequest removeSeatsRequest = helper.buildRemoveSeatsRequest("contractNumber", "subscriptionId", 1);
		assertThat(removeSeatsRequest.getActionName()).isEqualTo(OperationType.REMOVE_SEAT.getValue());
		assertThat(removeSeatsRequest.getContractNumber()).isEqualTo("contractNumber");
		assertThat(removeSeatsRequest.getLineItems().get(0).getSubscriptionId()).isEqualTo("subscriptionId");
		assertThat(removeSeatsRequest.getLineItems().get(0).getQuantity()).isEqualTo(1);
	}
}
