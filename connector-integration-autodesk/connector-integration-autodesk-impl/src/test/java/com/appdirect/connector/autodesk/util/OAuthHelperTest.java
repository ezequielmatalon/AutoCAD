package com.appdirect.connector.autodesk.util;

import org.assertj.core.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.appdirect.connector.autodesk.config.AutodeskConfiguration;
import com.appdirect.connector.autodesk.util.OAuthHelper;

public class OAuthHelperTest {

	@InjectMocks
	private OAuthHelper oauthHelper;

	@Mock
	private AutodeskConfiguration config;

	@BeforeMethod
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		Mockito.when(config.getKey()).thenReturn("key");
		Mockito.when(config.getSecret()).thenReturn("secret");
	}

	@Test(description = "Tests generateApiSignature")
	public void testGenerateApiSignature() {
		String token = "token";
		String timestamp = "timestamp";
		String generateApiSignature = oauthHelper.generateApiSignature(config, token, timestamp);
		Assertions.assertThat(generateApiSignature).isEqualTo("RWwf6rnhv0pu2CYxlOkN74HcaSicFDIoG6reIb09cCg=");
	}

	@Test(description = "Tests generateAuthorizationToken")
	public void testGenerateAuthorizationToken() {
		String token = oauthHelper.generateAuthorizationToken(config);
		Assertions.assertThat(token).isEqualTo("Basic a2V5OnNlY3JldA==");
	}

	@Test(description = "Tests generateAuthorizationToken")
	public void testGenerateOAuthSignature() {
		String timestamp = "timestamp";
		String signature = oauthHelper.generateOAuthSignature(config, timestamp);
		Assertions.assertThat(signature).isEqualTo("INTZWULmwDyFoDXhcEy0Ym19b/ENp/h2eGsyQ613kIc=");
	}
}
