package com.appdirect.connector.autodesk.rest.model.order.details;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;

public class OrderDetailsResponseDeserializerTest {

	@Mock
	private ObjectMapper mapper;

	@Mock
	private JsonParser parser;

	@Mock
	private DeserializationContext context;

	private OrderDetailsResponseDeserializer deserializer;

	@BeforeMethod
	@SuppressWarnings("serial")
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		deserializer = new OrderDetailsResponseDeserializer() {
			@Override
			protected ObjectMapper getMapper() {
				return mapper;
			}
		};
	}

	@Test(description = "Test deserialization of a success order details response")
	public void testDeserialize_success() throws JsonProcessingException, IOException {
		ObjectCodec codec = mock(ObjectCodec.class);
		JsonNode node = mock(JsonNode.class);
		JsonNode nodeOK = mock(JsonNode.class);
		JsonNode nodeMessage = mock(JsonNode.class);
		JsonParser messageParser = mock(JsonParser.class);
		Message message = mock(Message.class);

		when(mapper.readValue(messageParser, Message.class)).thenReturn(message);
		when(nodeMessage.traverse()).thenReturn(messageParser);
		when(nodeMessage.getNodeType()).thenReturn(JsonNodeType.OBJECT);
		when(node.get("message")).thenReturn(nodeMessage);
		when(nodeOK.asText()).thenReturn("OK");
		when(node.get("status")).thenReturn(nodeOK);
		when(codec.readTree(Mockito.isA(JsonParser.class))).thenReturn(node);
		when(parser.getCodec()).thenReturn(codec);

		OrderDetailsResponse deserialize = deserializer.deserialize(parser, context);
		assertThat(deserialize.isError()).isFalse();
		assertThat(deserialize.getErrorMessage()).isNull();
	}

	@Test(description = "Test deserialization of a failure order details response")
	public void testDeserialize_error() throws JsonProcessingException, IOException {
		ObjectCodec codec = mock(ObjectCodec.class);
		JsonNode node = mock(JsonNode.class);
		JsonNode nodeOK = mock(JsonNode.class);
		JsonNode nodeMessage = mock(JsonNode.class);

		when(nodeMessage.asText()).thenReturn("error message");
		when(nodeMessage.getNodeType()).thenReturn(JsonNodeType.STRING);
		when(node.get("message")).thenReturn(nodeMessage);
		when(nodeOK.asText()).thenReturn("OK");
		when(node.get("status")).thenReturn(nodeOK);
		when(codec.readTree(Mockito.isA(JsonParser.class))).thenReturn(node);
		when(parser.getCodec()).thenReturn(codec);

		OrderDetailsResponse deserialize = deserializer.deserialize(parser, context);
		assertThat(deserialize.isError()).isTrue();
		assertThat(deserialize.getErrorMessage()).isNotNull();
	}
}
