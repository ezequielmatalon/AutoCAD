package com.appdirect.connector.autodesk;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.Test;

import com.appdirect.connector.autodesk.config.Application;
import com.appdirect.connector.autodesk.config.AutodeskPersistenceConfiguration;
import com.appdirect.connector.autodesk.config.SwaggerConfig;
import com.appdirect.connector.autodesk.config.WebConfig;
import com.appdirect.connector.autodesk.entity.AutodeskOrder;
import com.appdirect.connector.autodesk.respository.AutodeskOrderRepository;
import com.google.common.collect.Lists;

@ContextConfiguration(initializers = ConfigFileApplicationContextInitializer.class,
		classes = { Application.class, WebConfig.class, AutodeskPersistenceConfiguration.class, SwaggerConfig.class })
@ActiveProfiles("test")
@WebAppConfiguration
public class AutodeskContextTest extends AbstractTestNGSpringContextTests {

	@Autowired
	AutodeskOrderRepository orderRepo;
	
	@Test(description = "Application context test")
	public void testContext() {
		Assertions.assertThat(true).isTrue();
	}
	
	@Test(description = "AutodeskOrder entity saving and finding test")
	public void crudOrder() {
		AutodeskOrder order = new AutodeskOrder();
		AutodeskOrder differentOrder = new AutodeskOrder("7", "1");
		order.setContractNumber("1");
		String poNumber = "2";
		order.setPoNumber(poNumber);
		
		orderRepo.save(order);
		orderRepo.save(differentOrder);
		
		AutodeskOrder savedOrder = orderRepo.findOne(poNumber);
		assertThat(savedOrder).isNotNull();
		assertThat(savedOrder.toString()).isNotEmpty();
		assertThat(savedOrder.equals(order)).isTrue();
		assertThat(savedOrder.hashCode()).isEqualTo(order.hashCode());
		assertThat(savedOrder.getContractNumber()).isNotEmpty();
		assertThat(savedOrder.getPoNumber()).isNotEmpty();

		Iterable<AutodeskOrder> itOrders = orderRepo.findAll();
		List<AutodeskOrder> orders = Lists.newArrayList(itOrders);
		assertThat(orders.isEmpty()).isFalse();
		assertThat(orders.size()).isEqualTo(2);
		assertThat(orders.get(0).equals(orders.get(1))).isFalse();
		assertThat(orders.get(0).hashCode()).isNotEqualTo(orders.get(1).hashCode());
		
		orderRepo.delete(poNumber);
		Boolean exists = orderRepo.exists(poNumber);
		assertThat(exists).isFalse();
	}
}

