package com.appdirect.connector.autodesk.service;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Collection;
import java.util.Optional;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.appdirect.connector.autodesk.exception.AutodeskAPIException;
import com.appdirect.connector.autodesk.exception.AutodeskServiceException;
import com.appdirect.connector.autodesk.model.AutodeskProduct;
import com.appdirect.connector.autodesk.rest.converter.DefaultConverter;
import com.appdirect.connector.autodesk.rest.model.products.price.Price;
import com.appdirect.connector.autodesk.rest.model.products.price.PriceResponse;
import com.appdirect.connector.autodesk.type.OperationType;
import com.appdirect.connector.autodesk.type.StatusType;
import com.appdirect.connector.autodesk.util.DateProvider;
import com.google.common.collect.Multimap;

import retrofit2.Call;
import retrofit2.Response;

public class AutodeskProductServiceImplTest {

	private static final String PRODUCT1 = "Product1";
	private static final String PRODUCT2 = "Product2";

	@Mock
	private AutodeskRestService restService;

	@Mock
	private Call<PriceResponse> priceCall;

	@Mock
	private DateProvider dateProvider;

	@Mock
	private DefaultConverter defaultConverter;

	@Mock
	private Multimap<String, AutodeskProduct> productMap;

	@InjectMocks
	private AutodeskProductServiceImpl service;

	@BeforeMethod
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test(description = "Testing the success behavior of the getPrice functionality in the service")
	public void testGetPrice_success() throws Throwable {
		PriceResponse result = new PriceResponse();
		result.setStatus(StatusType.ACCEPTED);
		String message="OK";
		result.setMessage(message);
		result.setStatusCode(200);
		Price price = new Price();
		price.setCurrency("$");
		price.setFormattedNetPrice("$400.0");
		price.setNetPrice(400.0d);
		result.setResponse(price);

		setup_getPrice("sku", result);
		result = service.getPrice("sku");
		assertThat(result.getStatus()).isEqualTo(StatusType.ACCEPTED);
		assertThat(result.getMessage()).isNotEmpty();
		assertThat(result.getStatusCode()).isEqualTo(200);
		Price priceResult = result.getResponse();
		assertThat(priceResult.getFormattedNetPrice()).isEqualTo(priceResult.getCurrency()+priceResult.getNetPrice());
	}

	@Test(expectedExceptions = AutodeskServiceException.class, description = "Testing the fail behavior of the getPrice functionality when a IOException is generated")
	public void testGetPrice_IOException() throws Exception {
		setup_getPrice_IOException("sku");
		service.getPrice("sku");
	}

	@Test(expectedExceptions = AutodeskAPIException.class, description = "Testing the fail behavior of the getPrice functionality when a AutodeskAPIException is generated")
	public void testGetPrice_APIException() throws Throwable {
		AutodeskAPIException exception = new AutodeskAPIException(1, "", "Exception message");
		setup_getPrice_AutodeskException("sku", exception);
		service.getPrice("sku");
	}

	@Test(description = "Testing get products")
	public void testGetProducts_success() {
		Collection<AutodeskProduct> productListNotEmpty = productListNotEmpty();
		when(productMap.values()).thenReturn(productListNotEmpty);

		Collection<AutodeskProduct> products = service.getProducts();
		assertThat(products).isNotEmpty();
	}

	@Test(description = "Testing get product by name")
	public void testGetProductsByName_success() {
		Collection<AutodeskProduct> productListNotEmpty = productListNotEmpty();
		when(productMap.get(PRODUCT1)).thenReturn(productListNotEmpty);

		Collection<AutodeskProduct> products = service.getProductsByName(PRODUCT1);
		assertThat(products).isEqualTo(productListNotEmpty);

		verify(productMap, times(1)).get(PRODUCT1);
	}

	@Test(description = "Testing get product by name and operation")
	public void testGetProductsByNameAndOperation_success() {
		Collection<AutodeskProduct> productListNotEmpty = productListNotEmpty();
		when(productMap.get(PRODUCT1)).thenReturn(productListNotEmpty);

		Optional<AutodeskProduct> product = service.getProductByNameAndOperation(PRODUCT1, OperationType.INITIAL_ORDER);
		assertThat(product.isPresent()).isTrue();

		verify(productMap, times(1)).get(PRODUCT1);
	}

	private void setup_getPrice(String sku, PriceResponse result) throws Throwable {
		Response<PriceResponse> response = null;

		when(dateProvider.getCurrentDate(DateProvider.DATE_ONLY_FORMAT)).thenReturn("date");
		when(restService.getPrice(null, null, sku, "date")).thenReturn(priceCall);
		when(priceCall.execute()).thenReturn(response);
		when(defaultConverter.process(response)).thenReturn(result);
	}

	private void setup_getPrice_IOException(String sku) throws IOException {
		IOException exception = new IOException("Exception message");

		when(dateProvider.getCurrentDate(DateProvider.DATE_ONLY_FORMAT)).thenReturn("date");
		when(restService.getPrice(null, null, sku, "date")).thenReturn(priceCall);
		when(priceCall.execute()).thenThrow(exception);
	}

	private void setup_getPrice_AutodeskException(String sku, Exception exception) throws Throwable {
		Response<PriceResponse> response = null;

		when(dateProvider.getCurrentDate(DateProvider.DATE_ONLY_FORMAT)).thenReturn("date");
		when(restService.getPrice(null, null, sku, "date")).thenReturn(priceCall);
		when(priceCall.execute()).thenReturn(response);
		when(defaultConverter.process(response)).thenThrow(exception);
	}

	private Collection<AutodeskProduct> productListNotEmpty() {
		AutodeskProduct product1 = mock(AutodeskProduct.class);
		AutodeskProduct product2 = mock(AutodeskProduct.class);

		when(product1.getProductName()).thenReturn(PRODUCT1);
		when(product2.getProductName()).thenReturn(PRODUCT2);

		when(product1.accepts(OperationType.INITIAL_ORDER)).thenReturn(true);
		when(product2.accepts(OperationType.INITIAL_ORDER)).thenReturn(false);

		return asList(product1);
	}

}
