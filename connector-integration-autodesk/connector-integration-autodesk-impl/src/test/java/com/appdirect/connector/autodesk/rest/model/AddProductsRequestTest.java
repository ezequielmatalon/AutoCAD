package com.appdirect.connector.autodesk.rest.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.test.util.ReflectionTestUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.appdirect.connector.autodesk.rest.model.order.common.LineItem;
import com.appdirect.connector.autodesk.rest.model.products.add.AddProductsRequest;

public class AddProductsRequestTest {

    private static final Double PRICE_1 = 100.50;
    private static final Double PRICE_2 = 255.72;
    private static final Double PRICE_TOTAL = PRICE_1 + PRICE_2;

    @Spy
    private AddProductsRequest request = AddProductsRequest.builder().build();

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddProductRequest_getNetAmount(){
        List<LineItem> itemList = new ArrayList<>();
        LineItem item1 = Mockito.mock(LineItem.class);
        LineItem item2 = Mockito.mock(LineItem.class);
        itemList.add(item1);
        itemList.add(item2);

        ReflectionTestUtils.setField(request, "lineItems", itemList);
        when(item1.getNetAmount()).thenReturn(PRICE_1.toString());
        when(item2.getNetAmount()).thenReturn(PRICE_2.toString());

        assertThat(request.getNetAmount()).isEqualTo(PRICE_TOTAL.toString());
    }

}
