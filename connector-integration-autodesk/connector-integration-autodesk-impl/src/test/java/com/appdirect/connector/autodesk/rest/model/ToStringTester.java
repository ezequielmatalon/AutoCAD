package com.appdirect.connector.autodesk.rest.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.PojoField;
import com.openpojo.validation.affirm.Affirm;
import com.openpojo.validation.test.Tester;
import com.openpojo.validation.utils.ValidationHelper;

public class ToStringTester implements Tester {

	private static final String DELIMITER = ", ";
	private static final String EQUALS = "=";
	private static final String CLOSE = ")";
	private static final String OPEN = "(";
	private static final String DOT = ".";
	private static final String NOT_ACTUAL_FIELD = "$jacocoData";

	@Override
	public void run(final PojoClass pojoClass) {
		final Object classInstance = ValidationHelper.getBasicInstance(pojoClass);
		String actualString = classInstance.toString();

		StringBuilder toStringBuilder = new StringBuilder();

		String packageName = classInstance.getClass().getPackage().getName() + DOT;
		String canonicalName = classInstance.getClass().getCanonicalName();

		String className = StringUtils.remove(canonicalName, packageName);

		toStringBuilder.append(className);
		toStringBuilder.append(OPEN);

		List<String> fieldsList = new ArrayList<>();

		for (final PojoField fieldEntry : pojoClass.getPojoFields()) {
			if (!StringUtils.equals(NOT_ACTUAL_FIELD, fieldEntry.getName())) {
				Object value = fieldEntry.get(classInstance);
				StringBuilder fieldBuilder = new StringBuilder();
				fieldBuilder.append(fieldEntry.getName());
				fieldBuilder.append(EQUALS);
				fieldBuilder.append(value);
				fieldsList.add(fieldBuilder.toString());
			}
		}
		toStringBuilder.append(String.join(DELIMITER, fieldsList));
		toStringBuilder.append(CLOSE);

		Affirm.affirmEquals("toString not equal for class " + className, actualString, toStringBuilder.toString());

	}
}
