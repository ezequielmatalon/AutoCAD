package com.appdirect.connector.autodesk.loader;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collection;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.appdirect.connector.autodesk.model.AutodeskProduct;
import com.appdirect.connector.autodesk.type.OperationType;
import com.google.common.collect.Multimap;

public class AutodeskProductLoaderTest {

	private static final String PRODUCTS_SHEET = "skus";
	private static final String PRODUCTS_SHEET_FIELD = "productsSheet";
	private static final String PRODUCTS_FILE = "test-skus.xlsx";
	private static final String PRODUCTS_FILE_INVALID = "test-skus-invalid.xlsx";
	private static final String PRODUCTS_FILE_FIELD = "productsFile";

	private static final String SKU = "128H1-WW6287-T317";
	private static final String PRODUCT_NAME = "Product1";
	private static final String OPERATION_TYPE_RAW = "Initial Order/Add Product";
	private static final String SKU_TYPE = "Recurring";
	private static final String TERM = "Monthly";

	@InjectMocks
	private AutodeskProductLoader productLoader;

	@Mock
	private Multimap<String, AutodeskProduct> productMap;

	@BeforeMethod
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		ReflectionTestUtils.setField(productLoader, PRODUCTS_FILE_FIELD, PRODUCTS_FILE);
		ReflectionTestUtils.setField(productLoader, PRODUCTS_SHEET_FIELD, PRODUCTS_SHEET);
	}

	@Test(description = "Tests that file is read successfully and content is the expected")
	public void testLoadProducts_success() throws IOException, URISyntaxException {
		Multimap<String, AutodeskProduct> productMap = productLoader.loadProducts();

		assertThat(productMap.isEmpty()).isFalse();
		assertThat(productMap.keySet().size()).isEqualTo(6);
		assertThat(productMap.values().size()).isEqualTo(36);

		Collection<AutodeskProduct> product1List = productMap.get(PRODUCT_NAME);

		assertThat(product1List).isNotEmpty();
		assertThat(product1List.size()).isEqualTo(6);

		AutodeskProduct product = product1List.iterator().next();

		assertThat(product).isNotNull();
		assertThat(product.getSku()).isEqualTo(SKU);
		assertThat(product.getProductName()).isEqualTo(PRODUCT_NAME);
		assertThat(product.getSkuType()).isEqualTo(SKU_TYPE);
		assertThat(product.getOperationTypeRaw()).isEqualTo(OPERATION_TYPE_RAW);
		assertThat(product.getOperationType()).isEqualTo(OperationType.INITIAL_ORDER);
		assertThat(product.getTerm()).isEqualTo(TERM);
	}

	@Test(expectedExceptions = IOException.class, description = "Tests that when an invalid file name is passed, an IOException is thrown")
	public void testLoadProducts_invalidFileName() throws IOException, URISyntaxException {
		ReflectionTestUtils.setField(productLoader, PRODUCTS_FILE_FIELD, PRODUCTS_FILE_INVALID);

		productLoader.loadProducts();
	}

	@Test(description = "Tests that load method is success and productMap bean is updated")
	@SuppressWarnings("unchecked")
	public void testLoad_succes() {
		boolean load = productLoader.load();

		assertThat(load).isTrue();

		verify(productMap, times(1)).clear();
		verify(productMap, times(1)).putAll(isA(Multimap.class));
	}

	@Test(description = "Tests that load method returns false when load was not success")
	public void testLoad_notSuccess() {
		ReflectionTestUtils.setField(productLoader, PRODUCTS_FILE_FIELD, PRODUCTS_FILE_INVALID);

		boolean load = productLoader.load();
		assertThat(load).isFalse();
	}
}
