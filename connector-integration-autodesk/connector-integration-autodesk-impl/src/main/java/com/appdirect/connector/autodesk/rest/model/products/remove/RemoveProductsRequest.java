package com.appdirect.connector.autodesk.rest.model.products.remove;

import java.util.List;

import lombok.Getter;
import lombok.experimental.Builder;

import com.appdirect.connector.autodesk.rest.model.order.common.LineItem;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Builder
public class RemoveProductsRequest {

	@JsonProperty("action_name")
	private String actionName;
	@JsonProperty("contract_number")
	private String contractNumber;
	@JsonProperty("line_items")
	private List<LineItem> lineItems;

}
