package com.appdirect.connector.autodesk.config.factory;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.appdirect.connector.autodesk.config.AutodeskConfiguration;
import com.appdirect.connector.autodesk.rest.model.AccessToken;
import com.appdirect.connector.autodesk.service.AutodeskOAuthService;
import com.appdirect.connector.autodesk.service.AutodeskRestService;
import com.appdirect.connector.autodesk.util.OAuthHelper;
import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Component
public class AutodeskRestServiceFactoryBean implements FactoryBean<AutodeskRestService> {

	@Autowired
	private OAuthHelper oauthHelper;

	@Autowired
	private AutodeskConfiguration config;

	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private AutodeskOAuthService autodeskOAuthService;

	@Override
	public AutodeskRestService getObject() throws Exception {
		HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
		logging.setLevel(HttpLoggingInterceptor.Level.BODY);
		OkHttpClient httpClient = new OkHttpClient.Builder().addInterceptor(chain -> {
			Request original = chain.request();
			AccessToken token = autodeskOAuthService.getToken().execute().body();
			String authorization = "Bearer " + token.getToken();
			String timestamp = oauthHelper.generateTimestamp();
			String signature = oauthHelper.generateApiSignature(config, token.getToken(), timestamp);

			Request.Builder requestBuilder = original.newBuilder().header("Authorization", authorization)
					.header("timestamp", timestamp).header("signature", signature)
					.method(original.method(), original.body());
			Request request = requestBuilder.build();
			return chain.proceed(request);
		}).addInterceptor(logging).build();
		return new Retrofit.Builder().baseUrl(config.getUrl()).client(httpClient)
				.addConverterFactory(JacksonConverterFactory.create(mapper))
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create()).build().create(AutodeskRestService.class);
	}

	@Override
	public Class<?> getObjectType() {
		return AutodeskRestService.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}
}
