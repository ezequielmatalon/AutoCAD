package com.appdirect.connector.autodesk.service;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.appdirect.connector.autodesk.exception.AutodeskRetryServiceException;
import com.appdirect.connector.autodesk.rest.model.AutodeskResponse;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.FailsafeException;
import net.jodah.failsafe.RetryPolicy;
import net.jodah.failsafe.function.Predicate;

@Service
public class AutodeskRetryServiceImpl implements AutodeskRetryService {

	@Value("${autodesk.retry.milliseconds}")
	private int millisecondsToRetry;

	@Value("${autodesk.retry.maxRetries}")
	private int maxRetries;

	@Autowired
	private Executor asyncExecutor;

	@Override
	public CompletableFuture<AutodeskResponse> getAsyncResponse(Callable<AutodeskResponse> callable, Consumer<AutodeskResponse> onSuccess, ResponseCallable doneCondition, ResponseCallable abortCondition) {
		CompletableFuture<AutodeskResponse> asyncFuture = CompletableFuture
				.supplyAsync(() -> Failsafe
						.with(getRetryPolicy(doneCondition, abortCondition))
						.get(() -> callable.call()), asyncExecutor)
				.whenComplete((response, failure) -> {
					if (doneCondition.call(response)) {
						onSuccess.accept(response);
					}
				});
		return asyncFuture;
	}

	@Override
	public <T> T getWithRetryCondition(Callable<T> callable, FailureCallable retryCondition) throws AutodeskRetryServiceException {
		try {
			return Failsafe.with(new RetryPolicy()
					.retryOn(f -> retryCondition.call(f))
					.withDelay(millisecondsToRetry, TimeUnit.MILLISECONDS)
					.withMaxRetries(maxRetries))
					.get(callable);
		} catch (FailsafeException e) {
			throw new AutodeskRetryServiceException("Callable fail, because retry policy exceed", e);
		}
	}

	private RetryPolicy getRetryPolicy(ResponseCallable doneCondition, ResponseCallable abortCondition) {
		return new RetryPolicy()
				.retryIf(retryPredicate(doneCondition))
				.abortIf(abortPredicate(abortCondition))
				.withDelay(millisecondsToRetry, TimeUnit.MILLISECONDS)
				.withMaxRetries(maxRetries);
	}

	private Predicate<AutodeskResponse> retryPredicate(ResponseCallable doneCondition) {
		return (response) -> !doneCondition.call(response);
	}

	private Predicate<AutodeskResponse> abortPredicate(ResponseCallable abortCondition) {
		return (response) -> abortCondition.call(response);
	}
}
