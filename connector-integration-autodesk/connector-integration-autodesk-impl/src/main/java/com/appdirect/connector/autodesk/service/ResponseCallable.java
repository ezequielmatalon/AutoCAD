package com.appdirect.connector.autodesk.service;

import com.appdirect.connector.autodesk.rest.model.AutodeskResponse;

@FunctionalInterface
public interface ResponseCallable {

	Boolean call(AutodeskResponse response);

}
