package com.appdirect.connector.autodesk.model;

import java.io.Serializable;

import com.appdirect.connector.autodesk.type.OperationType;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AutodeskProduct implements Serializable {

	private static final long serialVersionUID = 4561299942425132139L;

	final String sku;
	final String productName;
	final String skuType;
	final String operationTypeRaw;
	final OperationType operationType;
	final String term;

	@JsonIgnore
	public boolean accepts(OperationType operationType) {
		return getOperationType().acceptsOperation(operationType);
	}
}
