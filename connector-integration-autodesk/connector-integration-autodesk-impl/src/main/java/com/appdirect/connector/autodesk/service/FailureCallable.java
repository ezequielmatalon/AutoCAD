package com.appdirect.connector.autodesk.service;

@FunctionalInterface
public interface FailureCallable {

	Boolean call(Throwable t);
}
