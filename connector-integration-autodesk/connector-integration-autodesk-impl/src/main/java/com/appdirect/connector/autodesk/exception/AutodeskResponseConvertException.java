package com.appdirect.connector.autodesk.exception;

/**
 * This exception should be thrown when a response conversion error occurs
 */
public class AutodeskResponseConvertException extends Exception {

	private static final long serialVersionUID = 3616917713374838806L;

	public AutodeskResponseConvertException(Throwable t) {
		super(t);
	}

}
