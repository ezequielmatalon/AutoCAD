package com.appdirect.connector.autodesk.respository;

import org.springframework.data.repository.CrudRepository;

import com.appdirect.connector.autodesk.entity.AutodeskOrder;

public interface AutodeskOrderRepository extends CrudRepository<AutodeskOrder, String> {

}
