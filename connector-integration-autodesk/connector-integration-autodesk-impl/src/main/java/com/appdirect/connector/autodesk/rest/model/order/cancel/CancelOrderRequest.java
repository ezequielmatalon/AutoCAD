package com.appdirect.connector.autodesk.rest.model.order.cancel;

import lombok.Getter;
import lombok.experimental.Builder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Builder
public class CancelOrderRequest {

	@JsonProperty("action_name")
	private String actionName;
	@JsonProperty("contract_number")
	private String contractNumber;

}
