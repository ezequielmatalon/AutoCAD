package com.appdirect.connector.autodesk.rest.converter;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.appdirect.connector.autodesk.exception.AutodeskAPIException;
import com.appdirect.connector.autodesk.exception.AutodeskResponseConvertException;
import com.appdirect.connector.autodesk.rest.model.AutodeskError;
import com.fasterxml.jackson.databind.ObjectMapper;

import retrofit2.Response;

@Component
public class DefaultConverter {

	@Autowired
	private ObjectMapper mapper;

	protected <T> boolean isSuccessful(Response<T> response) {
		return response.isSuccessful();
	}

	protected <T> T success(Response<T> response) {
		return response.body();
	}

	protected <T> Throwable failure(Response<T> response) {
		try {
			AutodeskError error = mapper.readValue(response.errorBody().string(), AutodeskError.class);
			return new AutodeskAPIException(response.code(), error.getStatus(), error.getMessage());

		} catch (IOException e) {
			return new AutodeskResponseConvertException(e);
		}
	}

	public <T> T process(Response<T> response) throws Throwable {

		if (isSuccessful(response)) {
			return success(response);
		}

		throw failure(response);
	}

}
