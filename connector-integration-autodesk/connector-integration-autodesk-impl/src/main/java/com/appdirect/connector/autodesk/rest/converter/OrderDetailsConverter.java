package com.appdirect.connector.autodesk.rest.converter;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.appdirect.connector.autodesk.exception.AutodeskAPIException;
import com.appdirect.connector.autodesk.rest.model.order.details.OrderDetailsResponse;

import retrofit2.Response;

/**
 * Converter specific for order detail responses. Returns a {@link OrderDetailsResponse} object or throws an
 * {@link AutodeskAPIException}.
 */
@Component
public class OrderDetailsConverter extends DefaultConverter {

	@Override
	public <T> boolean isSuccessful(Response<T> response) {
		return response.isSuccessful() && !OrderDetailsResponse.class.cast(success(response)).isError();
	}

	@Override
	public <T> T success(Response<T> response) {
		return super.success(response);
	}

	@Override
	public <T> Throwable failure(Response<T> response) {
		if (!response.isSuccessful()) {
			return super.failure(response);

		} else {
			OrderDetailsResponse orderDetails = (OrderDetailsResponse) success(response);
			return new AutodeskAPIException(HttpStatus.NOT_FOUND.value(), orderDetails.getStatus().getValue(),
					orderDetails.getErrorMessage());
		}
	}

}
