
package com.appdirect.connector.autodesk.rest.model.order.details;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class OrderHeaderArray {

    @JsonProperty("contract_number")
    private String contractNumber;

}
