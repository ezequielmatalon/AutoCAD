package com.appdirect.connector.autodesk.rest.model.order.details;

import lombok.Getter;
import lombok.Setter;

import org.apache.commons.lang3.StringUtils;

import com.appdirect.connector.autodesk.rest.model.AutodeskResponse;
import com.appdirect.connector.autodesk.type.StatusType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@JsonDeserialize(using = OrderDetailsResponseDeserializer.class)
public class OrderDetailsResponse implements AutodeskResponse {

	@JsonProperty("status")
	private StatusType status;
	@JsonProperty("message")
	private Message message;

	/**
	 * Message field that should be mapped into an error message string
	 */
	private String errorMessage;

	/**
	 * @return whether this response was success or not
	 */
	@JsonIgnore
	public boolean isError() {
		return StringUtils.isNotBlank(errorMessage);
	}
	
	@JsonIgnore
	public String getContractNumber(){
		return message.getElements().get(0).getOrderHeaderArray().get(0).getContractNumber();
	}
}
