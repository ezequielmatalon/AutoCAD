
package com.appdirect.connector.autodesk.rest.model.common;

import lombok.Getter;
import lombok.Setter;

import com.appdirect.connector.autodesk.rest.model.AutodeskResponse;
import com.appdirect.connector.autodesk.type.StatusType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class RevokeResponse implements AutodeskResponse {

	@JsonProperty("status")
	private StatusType status;
	@JsonProperty("message")
	private String message;
	@JsonProperty("reference_number")
	private String referenceNumber;

}