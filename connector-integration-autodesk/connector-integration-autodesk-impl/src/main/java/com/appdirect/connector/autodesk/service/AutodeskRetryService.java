package com.appdirect.connector.autodesk.service;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import com.appdirect.connector.autodesk.exception.AutodeskRetryServiceException;
import com.appdirect.connector.autodesk.rest.model.AutodeskResponse;

public interface AutodeskRetryService {

	CompletableFuture<AutodeskResponse> getAsyncResponse(Callable<AutodeskResponse> callable, Consumer<AutodeskResponse> onSuccess, ResponseCallable doneCondition, ResponseCallable abortCondition);

	<T> T getWithRetryCondition(Callable<T> callable, FailureCallable retryCondition) throws AutodeskRetryServiceException;

}
