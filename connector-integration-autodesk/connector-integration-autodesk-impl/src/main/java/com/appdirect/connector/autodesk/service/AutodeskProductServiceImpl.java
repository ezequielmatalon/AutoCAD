package com.appdirect.connector.autodesk.service;

import java.util.Collection;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.appdirect.connector.autodesk.exception.AutodeskAPIException;
import com.appdirect.connector.autodesk.exception.AutodeskServiceException;
import com.appdirect.connector.autodesk.loader.AutodeskProductLoader;
import com.appdirect.connector.autodesk.model.AutodeskProduct;
import com.appdirect.connector.autodesk.rest.converter.DefaultConverter;
import com.appdirect.connector.autodesk.rest.model.products.price.PriceResponse;
import com.appdirect.connector.autodesk.type.OperationType;
import com.appdirect.connector.autodesk.util.DateProvider;
import com.google.common.collect.Multimap;

import retrofit2.Call;
import retrofit2.Response;

@Service
public class AutodeskProductServiceImpl implements AutodeskProductService {

	@Autowired
	private DateProvider dateProvider;

	@Autowired
	private AutodeskRestService restService;

	@Value("${autodesk.csn.recurring}")
	private String autodeskRecurring;

	@Autowired
	private DefaultConverter defaultConverter;

	@Autowired
	private AutodeskProductLoader productLoader;

	@Autowired
	private Multimap<String,AutodeskProduct> productMap;

	@PostConstruct
	public void init() {
		loadProducts();
	}

	@Override
	public boolean loadProducts() {
		return productLoader.load();
	}

	@Override
	public PriceResponse getPrice(String sku) throws AutodeskServiceException, AutodeskAPIException {
		try {
			String currentDate = dateProvider.getCurrentDate(DateProvider.DATE_ONLY_FORMAT);
			Call<PriceResponse> priceCall = restService.getPrice(autodeskRecurring, autodeskRecurring, sku,
					currentDate);
			Response<PriceResponse> priceResponse = priceCall.execute();

			return defaultConverter.process(priceResponse);
		} catch (AutodeskAPIException e) {
			throw e;
		} catch (Throwable t) {
			throw new AutodeskServiceException(t);
		}
	}

	@Override
	public Optional<AutodeskProduct> getProduct(String sku) {
		return getProducts().stream()
				.filter(p -> p.getSku().equals(sku))
				.findFirst();
	}

	@Override
	public Collection<AutodeskProduct> getProductsByName(String productName) {
		return productMap.get(productName);
	}

	@Override
	public Optional<AutodeskProduct> getProductByNameAndOperation(String productName, OperationType operationType) {
		return productMap.get(productName)
				.stream()
				.filter(p -> p.accepts(operationType)).findFirst();
	}

	@Override
	public Collection<AutodeskProduct> getProducts() {
		return productMap.values();
	}
}
