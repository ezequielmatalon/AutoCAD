package com.appdirect.connector.autodesk.rest.controller;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.appdirect.connector.autodesk.model.AutodeskProduct;
import com.appdirect.connector.autodesk.service.AutodeskProductService;
import com.appdirect.connector.autodesk.type.OperationType;

/**
 * Product controller for dev testing purposes
 */
@RestController
public class ProductController {

	@Autowired
	private AutodeskProductService productService;

	@RequestMapping("/products/load")
	public ResponseEntity<String> loadProducts() {

		boolean success = productService.loadProducts();

		if (success) {
			return new ResponseEntity<String>(HttpStatus.OK);
		} else {
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping("/products/{sku}")
	public AutodeskProduct getProductBySku(@PathVariable String sku) {
		return productService.getProduct(sku).get();
	}

	@RequestMapping("/products")
	public ResponseEntity<Collection<AutodeskProduct>> getProductsbyNameAndOperation(
			@RequestParam(value = "name", required = false) String productName,
			@RequestParam(value = "operation", required = false) String operation) {

		if (StringUtils.isNotBlank(productName) && StringUtils.isNotBlank(operation)) {
			OperationType operationType = OperationType.valueOf(operation);
			List<AutodeskProduct> list = productService.getProductByNameAndOperation(productName, operationType)
					.map(p -> Arrays.asList(p))
					.orElse(Collections.emptyList());
			return new ResponseEntity<Collection<AutodeskProduct>>(list, HttpStatus.OK);
		} else {
			return new ResponseEntity<Collection<AutodeskProduct>>(productService.getProducts(), HttpStatus.OK);
		}
	}
}
