
package com.appdirect.connector.autodesk.rest.model.order.common;

import lombok.Getter;
import lombok.experimental.Builder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Builder
public class LineItem {

	@JsonProperty("subscription_id")
	private String subscriptionId;
	@JsonProperty("part_number")
	private String partNumber;
	@JsonProperty("quantity")
	private Integer quantity;
	@JsonProperty("eu_first_name")
	private String euFirstName;
	@JsonProperty("eu_last_name")
	private String euLastName;
	@JsonProperty("eu_email")
	private String euEmail;
	@JsonProperty("eu_language")
	private String euLanguage;
	@JsonProperty("eu_country")
	private String euCountry;
	@JsonProperty("eu_primary_account_name")
	private String euPrimaryAccountName;
	@JsonProperty("eu_primary_account_address_line1")
	private String euPrimaryAccountAddressLine1;
	@JsonProperty("eu_primary_account_address_city")
	private String euPrimaryAccountAddressCity;
	@JsonProperty("eu_primary_account_address_state")
	private String euPrimaryAccountAddressState;
	@JsonProperty("eu_primary_account_address_country_code")
	private String euPrimaryAccountAddressCountryCode;
	@JsonProperty("eu_primary_account_address_postal_code")
	private String euPrimaryAccountAddressPostalCode;

	@JsonIgnore
	private Double price;

	@JsonProperty("net_amount")
	public String getNetAmount(){
		String netAmount = null;
		if (price != null) {
			netAmount = String.valueOf(price * quantity);
		}
		return netAmount;
	}
}
