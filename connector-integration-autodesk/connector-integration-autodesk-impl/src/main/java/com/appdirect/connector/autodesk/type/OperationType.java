package com.appdirect.connector.autodesk.type;

import lombok.Getter;

@Getter
public enum OperationType {
	INITIAL_ORDER("initial_order") {
		@Override
		public boolean acceptsOperation(OperationType operationType) {
			return super.acceptsOperation(operationType) || operationType.equals(ADD_PRODUCT);
		}
	},
	ADD_PRODUCT("add_products") {
		@Override
		public boolean acceptsOperation(OperationType operationType) {
			return super.acceptsOperation(operationType) || operationType.equals(INITIAL_ORDER);
		}
	},
	ADD_SEAT("add_seats"),
	REMOVE_SEAT("remove_seats"),
	REMOVE_PRODUCT("remove_products"),
	CANCEL_ORDER("cancel");

	private final String value;

	OperationType(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}

	public boolean acceptsOperation(OperationType operationType) {
		return operationType.equals(this);
	}
}
