
package com.appdirect.connector.autodesk.rest.model.order.status;

import java.util.Date;

import com.appdirect.connector.autodesk.rest.model.AutodeskResponse;
import com.appdirect.connector.autodesk.type.StatusType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class OrderStatusResponse implements AutodeskResponse {

	@JsonProperty("status")
	private StatusType status;
	@JsonProperty("message")
	private String message;
	@JsonProperty("last_updated")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss z")
	private Date lastUpdate;

}
