package com.appdirect.connector.autodesk.rest.model;

import com.appdirect.connector.autodesk.type.StatusType;

/**
 * Super type for every implementation of Autodesk API responses
 */
public interface AutodeskResponse {

	StatusType getStatus();

}
