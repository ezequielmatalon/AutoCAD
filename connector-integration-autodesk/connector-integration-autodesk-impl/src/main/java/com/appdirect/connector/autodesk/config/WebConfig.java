package com.appdirect.connector.autodesk.config;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.appdirect.connector.autodesk.model.AutodeskProduct;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

@Configuration
@EnableScheduling
@ComponentScan(basePackages = "com.appdirect.connector.autodesk")
public class WebConfig {
	@Value("${autodesk.consumer.key}")
	private String key;
	@Value("${autodesk.consumer.secret}")
	private String secret;
	@Value("${autodesk.url}")
	private String url;
	@Value("${autodesk.callback}")
	private String callback;
	@Value("${autodesk.retry.executorThreadNumber}")
	private int threadNumber;

	@Bean
	public AutodeskConfiguration config() {
		return AutodeskConfiguration.builder().key(key).secret(secret).url(url).callback(callback).build();
	}

	@Bean
	public ObjectMapper mapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper;
	}

	@Bean
	public Executor asyncExecutor() {
		return Executors.newScheduledThreadPool(threadNumber);
	}

	@Bean
	public Multimap<String, AutodeskProduct> productMap() {
		return Multimaps.synchronizedMultimap(ArrayListMultimap.create());
	}
}
