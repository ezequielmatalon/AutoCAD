package com.appdirect.connector.autodesk.util;

import java.io.IOException;
import java.util.Arrays;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.appdirect.connector.autodesk.rest.model.order.cancel.CancelOrderRequest;
import com.appdirect.connector.autodesk.rest.model.order.place.EndUser;
import com.appdirect.connector.autodesk.rest.model.order.common.LineItem;
import com.appdirect.connector.autodesk.rest.model.order.place.PlaceOrderRequest;
import com.appdirect.connector.autodesk.rest.model.products.add.AddProductsRequest;
import com.appdirect.connector.autodesk.rest.model.products.remove.RemoveProductsRequest;
import com.appdirect.connector.autodesk.rest.model.seats.add.AddSeatsRequest;
import com.appdirect.connector.autodesk.rest.model.seats.remove.RemoveSeatsRequest;
import com.appdirect.connector.autodesk.type.OperationType;

@Component
public class RequestBuilderHelper {

	@Value("${autodesk.csn.recurring}")
	private String autodeskRecurring;

	public AddSeatsRequest buildAddSeatsRequest(String poNumber, String contractNumber, String subscriptionId, String sku, Integer quantity, Double price)
			throws IOException {

		AddSeatsRequest addSeatsRequest = AddSeatsRequest.builder()
				.actionName(OperationType.ADD_SEAT.getValue())
				.contractNumber(contractNumber)
				.poNumber(poNumber)
				.lineItems(Arrays.asList(
						LineItem.builder()
						.subscriptionId(subscriptionId)
						.partNumber(sku)
						.quantity(quantity)
						.price(price)
						.build()
						)
				)
				.build();

		return addSeatsRequest;
	}

	public PlaceOrderRequest buildPlaceOrderRequest(String poNumber, String sku, Integer quantity, Double price) throws IOException {

		PlaceOrderRequest placeOrderRequest = PlaceOrderRequest.builder()
				.actionName(OperationType.INITIAL_ORDER.getValue())
				.poNumber(poNumber)
				.endUser(EndUser.builder()
						.firstName("Fu-Ch'un")
						.lastName("Hsiao")
						.email("test@abc.com")
						.language("JP")
						.country("JP")
						.primaryAccountName("name1")
						.primaryAccountAddressLine1("1 Some Big Road")
						.primaryAccountAddressCity("Tokyo")
						.primaryAccountAddressState("")
						.primaryAccountAddressCountryCode("JP")
						.primaryAccountAddressPostalCode("100-1234")
						.relatedAccountName("μ╡ïΦ»òσà¼σÅ╕")
						.relatedAccountAddressLine1("1 Some Big Road")
						.relatedAccountAddressCity("Tokyo")
						.relatedAccountAddressState("")
						.relatedAccountAddressCountryCode("JP")
						.relatedAccountAddressPostalCode("100-1234")
						.build()
				)
				.resellerAccountCsn(autodeskRecurring)
				.lineItems(Arrays.asList(
						LineItem.builder()
						.subscriptionId(UUID.randomUUID().toString())
						.partNumber(sku)
						.quantity(quantity)
						.price(price)
						.euFirstName("μùáσÉì")
						.euLastName("Φ╡╡")
						.euEmail("bomia6@email.com.sg")
						.euLanguage("CN")
						.euCountry("SG")
						.euPrimaryAccountName("Bomia Three's Account")
						.euPrimaryAccountAddressLine1("6 Lower Cross St")
						.euPrimaryAccountAddressCity("Singapore")
						.euPrimaryAccountAddressState("")
						.euPrimaryAccountAddressCountryCode("SG")
						.euPrimaryAccountAddressPostalCode("900003")
						.build()
						)
				)
				.build();

		return placeOrderRequest;
	}

	public CancelOrderRequest buildCancelOrderRequest(String contractNumber)
			throws IOException {
		CancelOrderRequest cancelOrderRequest = CancelOrderRequest.builder()
				.actionName(OperationType.CANCEL_ORDER.getValue())
				.contractNumber(contractNumber)
				.build();

		return cancelOrderRequest;
	}

	public RemoveProductsRequest buildRemoveProductsRequest(String contractNumber, String subscriptionId)
			throws IOException {
		RemoveProductsRequest removeProductsRequest = RemoveProductsRequest.builder()
				.actionName(OperationType.REMOVE_PRODUCT.getValue())
				.contractNumber(contractNumber)
				.lineItems(Arrays.asList(
						LineItem.builder()
						.subscriptionId(subscriptionId)
						.build()
						)
				)
				.build();

		return removeProductsRequest;
	}

	public RemoveSeatsRequest buildRemoveSeatsRequest(String contractNumber, String subscriptionId, Integer quantity)
			throws IOException {

		RemoveSeatsRequest removeSeatsRequest = RemoveSeatsRequest.builder()
				.actionName(OperationType.REMOVE_SEAT.getValue())
				.contractNumber(contractNumber)
				.lineItems(Arrays.asList(
						LineItem.builder()
						.subscriptionId(subscriptionId)
						.quantity(quantity)
						.build()
						)
				)
				.build();

		return removeSeatsRequest;
	}

	public AddProductsRequest buildAddProductsRequest(String poNumber, String contractNumber, String sku, Integer quantity, Double price)
			throws IOException {
		AddProductsRequest addProductsRequest = AddProductsRequest.builder()
				.actionName(OperationType.ADD_PRODUCT.getValue())
				.contractNumber(contractNumber)
				.poNumber(poNumber)
				.lineItems(Arrays.asList(
						LineItem.builder()
						.subscriptionId(UUID.randomUUID().toString())
						.partNumber(sku)
						.quantity(quantity)
						.price(price)
						.build()
						)
				)
				.build();

		return addProductsRequest;
	}
}
