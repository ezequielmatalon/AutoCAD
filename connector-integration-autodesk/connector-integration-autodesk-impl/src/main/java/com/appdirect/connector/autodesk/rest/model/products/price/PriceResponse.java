package com.appdirect.connector.autodesk.rest.model.products.price;

import lombok.Getter;
import lombok.Setter;

import com.appdirect.connector.autodesk.rest.model.AutodeskResponse;
import com.appdirect.connector.autodesk.type.StatusType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class PriceResponse implements AutodeskResponse {

	@JsonProperty("status_code")
	private Integer statusCode;
	@JsonProperty("status")
	private StatusType status;
	@JsonProperty("message")
	private String message;
	@JsonProperty("response")
	private Price response;
}
