package com.appdirect.connector.autodesk.service;

import com.appdirect.connector.autodesk.rest.model.AccessToken;

import retrofit2.Call;
import retrofit2.http.POST;

public interface AutodeskOAuthService {

	@POST("/v2/oauth/generateaccesstoken?grant_type=client_credentials")
	public Call<AccessToken> getToken();
}
