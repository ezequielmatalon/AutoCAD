package com.appdirect.connector.autodesk.loader;

import static java.nio.file.Files.newInputStream;
import static java.nio.file.Paths.get;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Iterator;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.appdirect.connector.autodesk.model.AutodeskProduct;
import com.appdirect.connector.autodesk.type.OperationType;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

@Slf4j
@Component
public class AutodeskProductLoader {

	private static final int SKU_INDEX = 1;
	private static final int PRODUCT_CODE_INDEX = 2;
	private static final int SKU_TYPE_INDEX = 3;
	private static final int OPERATION_TYPE_INDEX = 4;
	private static final int TERM_INDEX = 5;

	private static final String ADD_SEAT_REGEXP = "(?i:.*add seat.*)";
	private static final String INITIAL_ORDER_REGEXP = "(?i:.*initial order.*)";

	@Value("${autodesk.products.file}")
	private String productsFile;

	@Value("${autodesk.products.sheet}")
	private String productsSheet;

	@Autowired
	private Multimap<String, AutodeskProduct> productMap;

	/**
	 * Loads product from products file into database if needed.
	 *
	 * @return whether product list is updated
	 */
	public boolean load() {
		try {
			log.debug("Updating products from file {}", productsFile);

			Multimap<String, AutodeskProduct> products = loadProducts();

			synchronized (productMap) {
				productMap.clear();
				productMap.putAll(products);
			}

			log.info("Products updated successfully from file {}", productsFile);

			return true;

		} catch (IOException | URISyntaxException e) {
			log.error("Failed to read file {}", productsFile, e);
			return false;
		}
	}

	protected Multimap<String, AutodeskProduct> loadProducts() throws IOException, URISyntaxException {

		Multimap<String, AutodeskProduct> products = ArrayListMultimap.create();

		try (Workbook workbook = new XSSFWorkbook(newInputStream(getProductFilePath()))) {

			Sheet skuSheet = workbook.getSheet(productsSheet);

			boolean lastRow = false;

			Iterator<Row> rowIterator = skuSheet.rowIterator();

			// skip header
			if (rowIterator.hasNext()) {
				rowIterator.next();
			}

			while (rowIterator.hasNext() && !lastRow) {
				Row row = rowIterator.next();

				String sku = row.getCell(SKU_INDEX).getStringCellValue();
				String productName = row.getCell(PRODUCT_CODE_INDEX).getStringCellValue();
				String skuType = row.getCell(SKU_TYPE_INDEX).getStringCellValue();
				String operationTypeRaw = row.getCell(OPERATION_TYPE_INDEX).getStringCellValue();
				OperationType operationType = getOperationType(operationTypeRaw);
				String term = row.getCell(TERM_INDEX).getStringCellValue();

				if (StringUtils.isNotBlank(productName)) {
					products.put(productName, new AutodeskProduct(sku, productName, skuType, operationTypeRaw, operationType, term));
				} else {
					lastRow = true;
				}
			}
		}

		return products;
	}

	private OperationType getOperationType(String operationTypeRaw) {
		if (operationTypeRaw.matches(INITIAL_ORDER_REGEXP)) {
			return OperationType.INITIAL_ORDER;
		}

		if (operationTypeRaw.matches(ADD_SEAT_REGEXP)) {
			return OperationType.ADD_SEAT;
		}

		return OperationType.INITIAL_ORDER;
	}

	/**
	 * @return path object for products file
	 * @throws URISyntaxException
	 */
	private Path getProductFilePath() throws IOException, URISyntaxException {
		URL systemResource = this.getClass().getClassLoader().getResource(productsFile);
		if (systemResource == null) {
			throw new IOException(String.format("Failed to open products file %s", productsFile));
		}
		return get(systemResource.toURI());
	}
}
