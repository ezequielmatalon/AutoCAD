package com.appdirect.connector.autodesk.service;

import java.util.concurrent.CompletableFuture;

import com.appdirect.connector.autodesk.exception.AutodeskAPIException;
import com.appdirect.connector.autodesk.exception.AutodeskServiceException;
import com.appdirect.connector.autodesk.rest.model.AutodeskResponse;
import com.appdirect.connector.autodesk.rest.model.order.details.OrderDetailsResponse;
import com.appdirect.connector.autodesk.rest.model.order.status.OrderStatusResponse;

public interface AutodeskService {

	CompletableFuture<AutodeskResponse> placeOrder(String poNumber, String productName, Integer quantity) throws AutodeskServiceException, AutodeskAPIException;

	OrderStatusResponse getOrderStatus(String referenceNumber) throws AutodeskServiceException, AutodeskAPIException;

	CompletableFuture<AutodeskResponse> addSeats(String poNumber, String productName, Integer quantity)
			throws AutodeskServiceException, AutodeskAPIException;

	CompletableFuture<AutodeskResponse> cancelOrder(String poNumber) throws AutodeskServiceException, AutodeskAPIException;

	CompletableFuture<AutodeskResponse> removeProducts(String poNumber, String productName) throws AutodeskServiceException, AutodeskAPIException;

	CompletableFuture<AutodeskResponse> removeSeats(String poNumber, String productName, Integer quantity) throws AutodeskServiceException, AutodeskAPIException;

	CompletableFuture<AutodeskResponse> addProducts(String poNumber, String productName, Integer quantity) throws AutodeskServiceException, AutodeskAPIException;

	OrderDetailsResponse getOrderDetails(String poNumber) throws AutodeskServiceException, AutodeskAPIException;

}
