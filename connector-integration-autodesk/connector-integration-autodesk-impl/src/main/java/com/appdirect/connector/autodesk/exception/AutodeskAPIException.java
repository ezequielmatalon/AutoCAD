package com.appdirect.connector.autodesk.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * This class should be used to map an API error into an expected handled exception
 * 
 */
@Getter
@AllArgsConstructor
public class AutodeskAPIException extends Exception {

	private static final long serialVersionUID = 5331013638765503449L;

	/**
	 * API result code
	 */
	private int code;

	/**
	 * API response status field
	 */
	private String status;

	/**
	 * API response message field
	 */
	private String message;

}
