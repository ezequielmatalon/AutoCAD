package com.appdirect.connector.autodesk.rest.model.order.details;

import java.io.IOException;

import com.appdirect.connector.autodesk.type.StatusType;
import com.appdirect.connector.autodesk.util.ApplicationContextHolder;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * Custom deserializer for {@link OrderDetailsResponse} object
 */
public class OrderDetailsResponseDeserializer extends StdDeserializer<OrderDetailsResponse> {

	private static final long serialVersionUID = -964440615361792329L;

	private static final String STATUS = "status";
	private static final String MESSAGE = "message";

	private ObjectMapper mapper;

	public OrderDetailsResponseDeserializer() {
		this(null);
	}

	public OrderDetailsResponseDeserializer(Class<?> vc) {
		super(vc);
		mapper = getMapper();
	}

	protected ObjectMapper getMapper() {
		return ApplicationContextHolder.getContext().getBean(ObjectMapper.class);
	}

	@Override
	public OrderDetailsResponse deserialize(JsonParser parser, DeserializationContext context)
			throws IOException, JsonProcessingException {

		JsonNode node = parser.getCodec().readTree(parser);
		OrderDetailsResponse orderDetailsResponse = new OrderDetailsResponse();

		String status = node.get(STATUS).asText();
		orderDetailsResponse.setStatus(StatusType.valueOf(status));

		if (node.get(MESSAGE).isTextual()) {
			String errorMessage = node.get(MESSAGE).asText();
			orderDetailsResponse.setErrorMessage(errorMessage);

		} else {
			JsonParser messageNodeParser = node.get(MESSAGE).traverse();
			Message message = mapper.readValue(messageNodeParser, Message.class);
			orderDetailsResponse.setMessage(message);
		}

		return orderDetailsResponse;
	}

}
