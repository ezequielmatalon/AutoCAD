package com.appdirect.connector.autodesk.exception;

/**
 * Exception to be thrown in Autodesk service layer
 *
 */
public class AutodeskServiceException extends Exception {

	private static final long serialVersionUID = 8874249182662682004L;

	public AutodeskServiceException(Throwable t) {
		super(t);
	}

	public AutodeskServiceException(String message) {
		super(message);
	}
}
