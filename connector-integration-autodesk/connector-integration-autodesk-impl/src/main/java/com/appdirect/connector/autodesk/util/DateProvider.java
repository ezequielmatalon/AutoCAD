package com.appdirect.connector.autodesk.util;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Component;

@Component
public class DateProvider {

	private static final String UTC = "UTC";

	public static final String DATE_ONLY_FORMAT = "YYYY-MM-dd";

	public LocalDate getCurrentDate() {
		return LocalDate.now(ZoneId.of(UTC));
	}

	public String getCurrentDate(String pattern) {
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
		return dateFormatter.format(getCurrentDate());

	}
}
