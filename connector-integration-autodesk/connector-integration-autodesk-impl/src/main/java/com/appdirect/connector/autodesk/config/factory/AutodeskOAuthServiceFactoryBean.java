package com.appdirect.connector.autodesk.config.factory;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.appdirect.connector.autodesk.config.AutodeskConfiguration;
import com.appdirect.connector.autodesk.service.AutodeskOAuthService;
import com.appdirect.connector.autodesk.util.OAuthHelper;
import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Component
public class AutodeskOAuthServiceFactoryBean implements FactoryBean<AutodeskOAuthService> {

	@Autowired
	private AutodeskConfiguration config;

	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private OAuthHelper oauthHelper;

	@Override
	public AutodeskOAuthService getObject() throws Exception {
		HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
		logging.setLevel(HttpLoggingInterceptor.Level.BODY);
		OkHttpClient httpClient = new OkHttpClient.Builder().addInterceptor(chain -> {
			Request original = chain.request();
			String authorization = oauthHelper.generateAuthorizationToken(config);
			String timestamp = oauthHelper.generateTimestamp();
			String signature = oauthHelper.generateOAuthSignature(config, timestamp);

			Request.Builder requestBuilder = original.newBuilder().header("Authorization", authorization)
					.header("timestamp", timestamp).header("signature", signature)
					.method(original.method(), original.body());

			Request request = requestBuilder.build();
			return chain.proceed(request);
		}).addInterceptor(logging).build();

		return new Retrofit.Builder().baseUrl(config.getUrl()).client(httpClient)
				.addConverterFactory(JacksonConverterFactory.create(mapper))
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create()).build().create(AutodeskOAuthService.class);
	}

	@Override
	public Class<?> getObjectType() {
		return AutodeskOAuthService.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}
}
