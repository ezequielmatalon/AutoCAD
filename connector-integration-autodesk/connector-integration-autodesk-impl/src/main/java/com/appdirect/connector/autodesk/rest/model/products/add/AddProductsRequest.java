package com.appdirect.connector.autodesk.rest.model.products.add;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.experimental.Builder;

import com.appdirect.connector.autodesk.rest.model.order.common.LineItem;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Builder
public class AddProductsRequest {

	@JsonProperty("action_name")
	private String actionName;
	@JsonProperty("contract_number")
	private String contractNumber;
	@JsonProperty("po_number")
	private String poNumber;
	@JsonProperty("line_items")
	private List<LineItem> lineItems = new ArrayList<>();

	@JsonProperty("net_amount")
	public String getNetAmount(){
		return String.valueOf(lineItems.stream().mapToDouble(i->Double.valueOf(i.getNetAmount())).sum());
	}
}
