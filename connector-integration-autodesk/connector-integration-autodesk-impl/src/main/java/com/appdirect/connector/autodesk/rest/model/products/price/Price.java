package com.appdirect.connector.autodesk.rest.model.products.price;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class Price {

	@JsonProperty("net_price")
	private Double netPrice;
	@JsonProperty("currency")
	private String currency;
	@JsonProperty("formatted_net_price")
	private String formattedNetPrice;
}
