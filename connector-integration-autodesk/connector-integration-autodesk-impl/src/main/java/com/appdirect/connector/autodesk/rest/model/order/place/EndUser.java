
package com.appdirect.connector.autodesk.rest.model.order.place;

import lombok.Getter;
import lombok.experimental.Builder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Builder
public class EndUser {

	@JsonProperty("first_name")
	public String firstName;
	@JsonProperty("last_name")
	public String lastName;
	@JsonProperty("email")
	public String email;
	@JsonProperty("language")
	public String language;
	@JsonProperty("country")
	public String country;
	@JsonProperty("primary_account_name")
	public String primaryAccountName;
	@JsonProperty("primary_account_address_line1")
	public String primaryAccountAddressLine1;
	@JsonProperty("primary_account_address_city")
	public String primaryAccountAddressCity;
	@JsonProperty("primary_account_address_state")
	public String primaryAccountAddressState;
	@JsonProperty("primary_account_address_country_code")
	public String primaryAccountAddressCountryCode;
	@JsonProperty("primary_account_address_postal_code")
	public String primaryAccountAddressPostalCode;
	@JsonProperty("related_account_name")
	public String relatedAccountName;
	@JsonProperty("related_account_address_line1")
	public String relatedAccountAddressLine1;
	@JsonProperty("related_account_address_city")
	public String relatedAccountAddressCity;
	@JsonProperty("related_account_address_state")
	public String relatedAccountAddressState;
	@JsonProperty("related_account_address_country_code")
	public String relatedAccountAddressCountryCode;
	@JsonProperty("related_account_address_postal_code")
	public String relatedAccountAddressPostalCode;

}
