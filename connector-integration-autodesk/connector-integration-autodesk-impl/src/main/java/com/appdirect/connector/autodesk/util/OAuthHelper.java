package com.appdirect.connector.autodesk.util;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Component;

import com.appdirect.connector.autodesk.config.AutodeskConfiguration;

@Slf4j
@Component
public class OAuthHelper {

	private static final String HMAC_SHA256 = "HmacSHA256";

	public String generateAuthorizationToken(AutodeskConfiguration config) {
		String credentials = config.getKey() + ":" + config.getSecret();
		String token = "Basic " + new String(Base64.getEncoder().encode(credentials.getBytes()));
		return token;
	}

	public String generateTimestamp() {
		return String.valueOf(Instant.now().toEpochMilli()).substring(0, 10);
	}

	/**
	 * Base64Encode(HMACSHA256(Concatenate (callbackAddress, client_id,timestamp))
	 *
	 * @param config
	 * @param timestamp
	 * @return
	 */
	public String generateOAuthSignature(AutodeskConfiguration config, String timestamp) {
		StringBuilder sb = new StringBuilder();
		sb.append(config.getCallback());
		sb.append(config.getKey());
		sb.append(timestamp);
		return generateSignature(config, sb.toString());
	}

	/**
	 * signature = Base64Encode(HMACSHA256(Concatenate (callbackAddress,
	 * Access_Token from the service(no bearer string here), timestamp))
	 *
	 * @param config
	 * @param timestamp
	 * @return
	 */
	public String generateApiSignature(AutodeskConfiguration config, String token, String timestamp) {
		StringBuilder sb = new StringBuilder();
		sb.append(config.getCallback());
		sb.append(token);
		sb.append(timestamp);
		return generateSignature(config, sb.toString());
	}

	private String generateSignature(AutodeskConfiguration config, String message) {
		return new String(Base64.getEncoder().encode(hmacSHA256(config.getSecret(), message)));
	}

	private byte[] hmacSHA256(String secret, String message) {
		try {

			Mac sha256HMAC = Mac.getInstance(HMAC_SHA256);
			SecretKeySpec secretKey = new SecretKeySpec(secret.getBytes(), HMAC_SHA256);
			sha256HMAC.init(secretKey);

			return sha256HMAC.doFinal(message.getBytes());
		} catch (IllegalStateException | InvalidKeyException | NoSuchAlgorithmException e) {
			log.error(e.getMessage());
			return new byte[0];
		}
	}
}
