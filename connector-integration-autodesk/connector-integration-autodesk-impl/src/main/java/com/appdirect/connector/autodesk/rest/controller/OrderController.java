package com.appdirect.connector.autodesk.rest.controller;

import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpServletRequest;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.appdirect.connector.autodesk.exception.AutodeskAPIException;
import com.appdirect.connector.autodesk.exception.AutodeskServiceException;
import com.appdirect.connector.autodesk.rest.model.AutodeskResponse;
import com.appdirect.connector.autodesk.rest.model.order.status.OrderStatusResponse;
import com.appdirect.connector.autodesk.service.AutodeskProductService;
import com.appdirect.connector.autodesk.service.AutodeskService;

@RestController
@Slf4j
public class OrderController {

	@Autowired
	private AutodeskService service;

	@Autowired
	private AutodeskProductService productService;

	@RequestMapping(value = "/order", method = RequestMethod.POST)
	public ResponseEntity<String> placerOrder(@RequestParam String poNumber, @RequestParam String productName, @RequestParam Integer quantity) throws AutodeskServiceException, AutodeskAPIException {

		CompletableFuture<AutodeskResponse> placeOrder = service.placeOrder(poNumber, productName, quantity);
		placeOrder.thenRun(() -> log.debug("Place Order request completed successfully"));

		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}

	@RequestMapping(value = "/order/{poNumber}", method = RequestMethod.DELETE)
	public ResponseEntity<String> cancelOrder(@PathVariable String poNumber) throws AutodeskServiceException, AutodeskAPIException {

		CompletableFuture<AutodeskResponse> cancelOrder = service.cancelOrder(poNumber);
		cancelOrder.thenRun(() -> log.debug("Cancel Order request completed successfully"));

		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}

	@RequestMapping(value = "/order/{poNumber}/product/{productName}", method = RequestMethod.DELETE)
	public ResponseEntity<String> removeProducts(@PathVariable String poNumber, @PathVariable String productName) throws AutodeskServiceException, AutodeskAPIException {

		CompletableFuture<AutodeskResponse> removeProducts = service.removeProducts(poNumber, productName);
		removeProducts.thenRun(() -> log.debug("Remove Products request completed successfully"));

		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}

	@RequestMapping(value = "/order/{poNumber}/product/{productName}/seat", method = RequestMethod.DELETE)
	public ResponseEntity<String> removeSeats(@PathVariable String poNumber, @PathVariable String productName, @RequestParam Integer quantity) throws AutodeskServiceException, AutodeskAPIException {

		CompletableFuture<AutodeskResponse> removeSeats = service.removeSeats(poNumber, productName, quantity);
		removeSeats.thenRun(() -> log.debug("Remove Seats request completed successfully"));

		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}

	@RequestMapping("/status/{referenceNumber}")
	public OrderStatusResponse getOrderStatus(@PathVariable String referenceNumber)
			throws AutodeskServiceException, AutodeskAPIException {
		return service.getOrderStatus(referenceNumber);
	}

	@RequestMapping(value = "/order/{poNumber}", method = RequestMethod.GET)
	public AutodeskResponse getOrderDetails(@PathVariable("poNumber") String poNumber)
			throws AutodeskServiceException, AutodeskAPIException {
		return service.getOrderDetails(poNumber);
	}

	@RequestMapping(value = "/order/{poNumber}/product", method = RequestMethod.POST)
	public ResponseEntity<String> addProducts(@PathVariable String poNumber, @RequestParam String productName, @RequestParam Integer quantity)
			throws AutodeskServiceException, AutodeskAPIException, AutodeskAPIException {

		CompletableFuture<AutodeskResponse> addProducts = service.addProducts(poNumber, productName, quantity);
		addProducts.thenRun(() -> log.debug("Add Products request completed successfully"));

		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}

	@RequestMapping(value = "/order/{poNumber}/product/{productName}/seat", method = RequestMethod.POST)
	public ResponseEntity<String> addSeats(@PathVariable String poNumber, @PathVariable String productName, @RequestParam Integer quantity) throws AutodeskServiceException, AutodeskAPIException {

		CompletableFuture<AutodeskResponse> addSeats = service.addSeats(poNumber, productName, quantity);
		addSeats.thenRun(() -> log.debug("Add Seats request completed successfully"));

		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}

	@RequestMapping("/price/{sku}/")
	public AutodeskResponse getPrice(@PathVariable String sku) throws AutodeskServiceException, AutodeskAPIException {
		return productService.getPrice(sku);
	}

	@ExceptionHandler(AutodeskAPIException.class)
	public ResponseEntity<String> apiExceptionHandler(HttpServletRequest req, AutodeskAPIException e) {
		StringBuilder sb = new StringBuilder();
		sb.append("Status: ").append(e.getStatus()).append(", ");
		sb.append("Message : ").append(e.getMessage());
		return new ResponseEntity<>(sb.toString(), HttpStatus.valueOf(e.getCode()));
	}

	@ExceptionHandler(AutodeskServiceException.class)
	public ResponseEntity<String> serviceExceptionHandler(HttpServletRequest req, AutodeskServiceException e) {
		return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
