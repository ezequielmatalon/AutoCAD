package com.appdirect.connector.autodesk.type;

import com.fasterxml.jackson.annotation.JsonValue;

public enum StatusType {
	OK("OK"),
	FAILED("FAILED"),
	ACCEPTED("Order Accepted");

	private final String value;

	StatusType(String value){
		this.value = value;
	}

	@JsonValue
	public String getValue() {
		return value;
	}

}
