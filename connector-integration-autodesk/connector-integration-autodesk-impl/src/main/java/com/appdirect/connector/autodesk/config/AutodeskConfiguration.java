package com.appdirect.connector.autodesk.config;

import lombok.Getter;
import lombok.experimental.Builder;

@Builder
@Getter
public class AutodeskConfiguration {
	private String key;
	private String secret;
	private String url;
	private String callback;

}
