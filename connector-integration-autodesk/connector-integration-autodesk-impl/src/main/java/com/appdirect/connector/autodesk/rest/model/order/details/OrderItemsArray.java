
package com.appdirect.connector.autodesk.rest.model.order.details;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class OrderItemsArray {

    @JsonProperty("sku")
    private String sku;
    @JsonProperty("quantity")
    private Integer quantity;
    @JsonProperty("seats")
    private String seats;
    @JsonProperty("license_description")
    private String licenseDescription;
    @JsonProperty("subs_id")
    private String subsId;
    @JsonProperty("sales_license_type")
    private String salesLicenseType;

}
