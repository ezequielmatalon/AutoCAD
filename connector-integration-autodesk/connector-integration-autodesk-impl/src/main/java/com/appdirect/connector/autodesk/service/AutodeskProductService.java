package com.appdirect.connector.autodesk.service;

import java.util.Collection;
import java.util.Optional;

import com.appdirect.connector.autodesk.exception.AutodeskAPIException;
import com.appdirect.connector.autodesk.exception.AutodeskServiceException;
import com.appdirect.connector.autodesk.model.AutodeskProduct;
import com.appdirect.connector.autodesk.rest.model.products.price.PriceResponse;
import com.appdirect.connector.autodesk.type.OperationType;

public interface AutodeskProductService {

	PriceResponse getPrice(String sku) throws AutodeskServiceException, AutodeskAPIException;

	boolean loadProducts();

	Collection<AutodeskProduct> getProducts();

	Optional<AutodeskProduct> getProduct(String sku);

	Collection<AutodeskProduct> getProductsByName(String productName);

	Optional<AutodeskProduct> getProductByNameAndOperation(String productName, OperationType operationType);

}
