
package com.appdirect.connector.autodesk.rest.model.order.details;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class Element {

    @JsonProperty("order_header_array")
    private List<OrderHeaderArray> orderHeaderArray = new ArrayList<OrderHeaderArray>();
    @JsonProperty("order_items_array")
    private List<OrderItemsArray> orderItemsArray = new ArrayList<OrderItemsArray>();

}
