package com.appdirect.connector.autodesk.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="autodesk_order")
public class AutodeskOrder implements Serializable {

	private static final long serialVersionUID = 3317438763909948389L;

	@Id
	@Column(length = 36, name = "po_number")
	private String poNumber;

	@Column(length = 16, name = "contract_number")
	private String contractNumber;

}
