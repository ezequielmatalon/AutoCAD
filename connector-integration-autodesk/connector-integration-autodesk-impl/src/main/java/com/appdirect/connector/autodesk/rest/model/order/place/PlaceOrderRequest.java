package com.appdirect.connector.autodesk.rest.model.order.place;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.experimental.Builder;

import com.appdirect.connector.autodesk.rest.model.order.common.LineItem;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Builder
public class PlaceOrderRequest{

	@JsonProperty("action_name")
	private String actionName;
	@JsonProperty("po_number")
	private String poNumber;
	@JsonProperty("end_user")
	private EndUser endUser;
	@JsonProperty("reseller_account_csn")
	private String resellerAccountCsn;
	@JsonProperty("line_items")
	private List<LineItem> lineItems = new ArrayList<>();

}
