package com.appdirect.connector.autodesk.service;

import com.appdirect.connector.autodesk.rest.model.order.common.RevokeResponse;
import com.appdirect.connector.autodesk.rest.model.order.cancel.CancelOrderRequest;
import com.appdirect.connector.autodesk.rest.model.order.details.OrderDetailsResponse;
import com.appdirect.connector.autodesk.rest.model.order.place.PlaceOrderRequest;
import com.appdirect.connector.autodesk.rest.model.order.place.PlaceOrderResponse;
import com.appdirect.connector.autodesk.rest.model.order.status.OrderStatusResponse;
import com.appdirect.connector.autodesk.rest.model.products.add.AddProductsRequest;
import com.appdirect.connector.autodesk.rest.model.products.add.AddProductsResponse;
import com.appdirect.connector.autodesk.rest.model.products.price.PriceResponse;
import com.appdirect.connector.autodesk.rest.model.products.remove.RemoveProductsRequest;
import com.appdirect.connector.autodesk.rest.model.seats.add.AddSeatsRequest;
import com.appdirect.connector.autodesk.rest.model.seats.add.AddSeatsResponse;
import com.appdirect.connector.autodesk.rest.model.seats.remove.RemoveSeatsRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface AutodeskRestService {
	@POST("/v1/orders/fulfillment")
	public Call<PlaceOrderResponse> placeOrder(@Header("CSN") String csn, @Body PlaceOrderRequest req);

	@GET("/v1/orders/status/{referenceNumber}")
	public Call<OrderStatusResponse> getOrderStatus(@Header("CSN") String csn,
			@Path("referenceNumber") String referenceNumber);

	@POST("/v1/orders/fulfillment")
	public Call<AddSeatsResponse> addSeats(@Header("CSN") String csn, @Body AddSeatsRequest req);

	@POST("/v1/orders/revoke")
	public Call<RevokeResponse> cancelOrder(@Header("CSN") String csn, @Body CancelOrderRequest req);

	@POST("/v1/orders/revoke")
	public Call<RevokeResponse> removeProducts(@Header("CSN") String csn, @Body RemoveProductsRequest req);

	@POST("/v1/orders/revoke")
	public Call<RevokeResponse> removeSeats(@Header("CSN") String csn, @Body RemoveSeatsRequest req);

	@GET("/v1/orders")
	public Call<OrderDetailsResponse> getOrderDetails(@Header("CSN") String csn, @Query("customer_number") String customerNumber, @Query("partner_po") String poNumber);

	@POST("/v1/orders/fulfillment")
	public Call<AddProductsResponse> addProducts(@Header("CSN") String csn, @Body AddProductsRequest req);

	@GET("/v1/sku/prices")
	public Call<PriceResponse> getPrice(@Header("CSN") String csn, @Query("customer_number") String customerNumber, @Query("part_number") String sku, @Query("price_date") String date);

}
