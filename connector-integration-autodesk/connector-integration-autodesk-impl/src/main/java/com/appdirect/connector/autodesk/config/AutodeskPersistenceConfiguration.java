package com.appdirect.connector.autodesk.config;

import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.appdirect.connector.autodesk.AutodeskModule;

@EntityScan(basePackageClasses = AutodeskModule.class)
@EnableJpaRepositories(basePackageClasses = AutodeskModule.class)
@Configuration
public class AutodeskPersistenceConfiguration {
	protected AutodeskPersistenceConfiguration() {
	}
}
