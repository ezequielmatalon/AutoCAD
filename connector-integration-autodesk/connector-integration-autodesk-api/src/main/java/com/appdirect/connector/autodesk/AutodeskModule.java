package com.appdirect.connector.autodesk;

/**
 * Spring package discovery marker
 */
public final class AutodeskModule {
	private AutodeskModule() {
	}
}
